#ifndef WOWOBJECT_H
#define WOWOBJECT_H
#include <QMainWindow>

class WowObject : public QMainWindow
{
    Q_OBJECT

public:
    ulong Guid = 0;
    ulong SummonedBy = 0;
    float XPos = 0;
    float YPos = 0;
    float ZPos = 0;
    float Rotation = 0;
    uint BaseAddress = 0;
    uint UnitFieldsAddress = 0;
    uint Type = 0;
    QString Continent = "";
    QString MapName = "";
    QString Name = "";
    QString objType = "";
    QVector<int> buffs;
    uint CurrentHealth = 0;
    uint MaxHealth = 0;
    uint CurrentEnergy = 0; // mana, rage or energy
    uint MaxEnergy = 0;
    uint Level = 0;
    uint UnitID = 0;
    uint Faction;
};

#endif // WOWOBJECT_H
