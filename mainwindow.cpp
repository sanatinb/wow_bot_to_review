#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include "QtGlobal"
#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include <winuser.h>
#include <QThread>
#include <QKeyEvent>
#include <QDateTime>
#include <QShortcut>
#include "QTextCodec"

void PressKey(ushort scancode);

static int TimerMS = 400, PlayerInfo = 0, NewNodesCounter = 0, Jumps = 0;
static int StartMode =0, NoNode=1;
static bool IsLoading=false, JaineMode = false;

static QSqlQuery *query, *secondQuery;
static QSqlDatabase  db;
static QString ctr, TableViewMode="", UserName, Password;
static HANDLE hProc;
static HWND hCurWnd;

static int Looted=0, NoLootedIzzaPlayers=0;

DWORD RM(DWORD Addr){
    DWORD val;
    ReadProcessMemory(hProc, reinterpret_cast<LPCVOID>(Addr), &val, sizeof(val), nullptr);
    return val;
}

float RMfloat(DWORD Addr){
    float val;
    ReadProcessMemory(hProc, reinterpret_cast<LPCVOID>(Addr), &val, sizeof(val), nullptr);
    return val;
}

int RMint(DWORD Addr){
    int val;
    ReadProcessMemory(hProc, reinterpret_cast<LPCVOID>(Addr), &val, sizeof(val), nullptr);
    return val;
}

uint RMuint(DWORD Addr){
    uint val;
    ReadProcessMemory(hProc, reinterpret_cast<LPCVOID>(Addr), &val, sizeof(val), nullptr);
    return val;
}

QString RMstring(DWORD Addr) {
    char x[255];
    ReadProcessMemory(hProc, reinterpret_cast<LPCVOID>(Addr), &x, 255, nullptr);
    return QString(x);
}

static uint ObjectManager = 0;
static uint ClientConnection = 0;
static uint FirstObject = 0;
static uint TotalWowObjects = 0;
static uint TotalNpc = 0,TotalPlayer = 0,TotalNode = 0;
//static float ZPosRange = 1000;
//static int CurContinent = 0;

//static bool etrieveRadarIsReady = false;

static QVector<int> objInt,profLvl;
static QStringList objStr, objType;

static DWORD adresCharX, adresCharY, adresCharZ, adresCharOX;

void initEnum(){
objStr<<"Малая ториевая жила";objInt<<324; objType<<"MINE";profLvl<<245;  // Small Thorium Vein
objStr<<"Ароматитовая жила";objInt<<1610; objType<<"MINE";profLvl<<65;  // Incendicite Mineral Vein
objStr<<"Медная жила";objInt<<1731; objType<<"MINE";profLvl<<1;  // Copper Vein
objStr<<"Оловянная жила";objInt<<1732; objType<<"MINE";profLvl<<65;  // Tin Vein
objStr<<"Серебряная жила"; objInt<<1733; objType<<"MINE";profLvl<<75;  // Silver Vein
objStr<<"Золотая жила"; objInt<<1734; objType<<"MINE";profLvl<<155;  // Gold Vein
objStr<<"Залежи железа";objInt<<1735; objType<<"MINE";profLvl<<125;  // Iron Deposit
objStr<<"Мифриловые залежи";objInt<<2040; objType<<"MINE";profLvl<<175;  // Mithril Deposit
objStr<<"Залежи истинного серебра"; objInt<<2047; objType<<"MINE";profLvl<<230;  // Truesilver Deposit
objStr<<"Малое месторождение кровавого камня";objInt<<2653; objType<<"MINE";profLvl<<75;  // Lesser Bloodstone Deposit
objStr<<"Индарилиевая жила";objInt<<19903; objType<<"MINE";profLvl<<150;  // Indurium Mineral Vein
objStr<<"Покрытая слизью серебряная жила"; objInt<<73940; objType<<"MINE";profLvl<<75;  // Ooze Covered Silver Vein
objStr<<"Покрытая слизью золотая жила"; objInt<<73941; objType<<"MINE";profLvl<<155;  // Ooze Covered Gold Vein
objStr<<"Покрытые слизью залежи истинного серебра"; objInt<<123309; objType<<"MINE";profLvl<<230;  // Ooze Covered Truesilver Deposit
objStr<<"Покрытые слизью мифриловые залежи";objInt<<123310; objType<<"MINE";profLvl<<175;  // Ooze Covered Mithril Deposit
objStr<<"Покрытая слизью ториевая жила";objInt<<123848; objType<<"MINE";profLvl<<230;  // Ooze Covered Thorium Vein
objStr<<"Залежи черного железа";objInt<<165658; objType<<"MINE";profLvl<<230;  // Dark Iron Deposit
objStr<<"Богатая ториевая жила";objInt<<175404; objType<<"MINE";profLvl<<255;  // Rich Thorium Vein
objStr<<"Покрытая слизью богатая ториевая жила"; objInt<<177388; objType<<"MINE";profLvl<<255;  // Ooze Covered Rich Thorium Vein
objStr<<"Месторождение оскверненного железа"; objInt<<181555; objType<<"MINE";profLvl<<275;  // Fel Iron Deposit
objStr<<"Залежи адамантита";objInt<<181556; objType<<"MINE";profLvl<<325;  // Adamantite Deposit
objStr<<"Кориевая жила"; objInt<<181557; objType<<"MINE";profLvl<<375;  // Khorium Vein
objStr<<"Богатые залежи адамантита";objInt<<181569; objType<<"MINE";profLvl<<350;  // Rich Adamantite Deposit
objStr<<"Месторождение хаотита";objInt<<185877; objType<<"MINE";profLvl<<275;  // Nethercite Deposit
objStr<<"Залежи кобальта";objInt<<189978; objType<<"MINE";profLvl<<350;  // Cobalt Deposit
objStr<<"Богатые залежи кобальта"; objInt<<189979; objType<<"MINE";profLvl<<375;  // Rich Cobalt Deposit
objStr<<"Месторождение саронита";objInt<<189980; objType<<"MINE";profLvl<<400;  // Saronite Deposit
objStr<<"Богатое месторождение саронита";objInt<<189981; objType<<"MINE";profLvl<<425;  // Rich Saronite Deposit
objStr<<"Залежи титана";objInt<<191133; objType<<"MINE";profLvl<<450;  // Titanium Vein

objStr<<"Сребролист";           objInt<<1617;objType<<"HERB";profLvl<<1;  // Silverleaf
objStr<<"Мироцвет";             objInt<<1618;objType<<"HERB";profLvl<<1;  // Peacebloom
objStr<<"Земляной корень";      objInt<<1619;objType<<"HERB";profLvl<<15;  // Earthroot
objStr<<"Магороза";             objInt<<1620;objType<<"HERB";profLvl<<50;  // Mageroyal
objStr<<"Остротерн";            objInt<<1621;objType<<"HERB";profLvl<<70;  // Briarthorn
objStr<<"Синячник";             objInt<<1622;objType<<"HERB";profLvl<<100;  // Bruiseweed
objStr<<"Дикий сталецвет";      objInt<<1623;objType<<"HERB";profLvl<<115;  // Wild Steelbloom
objStr<<"Королевская кровь";    objInt<<1624;objType<<"HERB";profLvl<<125;  // Kingsblood
objStr<<"Могильный мох";        objInt<<1628;objType<<"HERB";profLvl<<120;  // Grave Moss
objStr<<"Корень жизни";         objInt<<2041;objType<<"HERB";profLvl<<150;  // Liferoot
objStr<<"Бледнолист";           objInt<<2042;objType<<"HERB";profLvl<<160;  // Fadeleaf
objStr<<"Кадгаров ус";          objInt<<2043;objType<<"HERB";profLvl<<185;  // Khadgar's Whisker
objStr<<"Морозник";             objInt<<2044;objType<<"HERB";profLvl<<195;  // Wintersbite
objStr<<"Удавник";              objInt<<2045;objType<<"HERB";profLvl<<85;  // Stranglekelp
objStr<<"Златошип";             objInt<<2046;objType<<"HERB";profLvl<<170;  // Goldthorn
objStr<<"Огнецвет";             objInt<<2866;objType<<"HERB";profLvl<<205;  // Firebloom
objStr<<"Лиловый лотос";        objInt<<142140;objType<<"HERB";profLvl<<210;  // Purple Lotus
objStr<<"Слезы Артаса";         objInt<<142141;objType<<"HERB";profLvl<<220;  // Arthas' Tears
objStr<<"Солнечник";            objInt<<142142;objType<<"HERB";profLvl<<230;  // Sungrass
objStr<<"Пастушья сумка";       objInt<<142143;objType<<"HERB";profLvl<<235;  // Blindweed
objStr<<"Призрачная поганка";   objInt<<142144;objType<<"HERB";profLvl<<245;  // Ghost Mushroom
objStr<<"Кровь Грома";          objInt<<142145;objType<<"HERB";profLvl<<250;  // Gromsblood
objStr<<"Золотой сансам";       objInt<<176583;objType<<"HERB";profLvl<<260;  // Golden Sansam
objStr<<"Снолист";              objInt<<176584;objType<<"HERB";profLvl<<270;  // Dreamfoil
objStr<<"Горный серебряный шалфей"; objInt<<176586;objType<<"HERB";profLvl<<280;  // Mountain Silversage
objStr<<"Чумоцвет";             objInt<<176587;objType<<"HERB";profLvl<<285;  // Plaguebloom
objStr<<"Ледяной зев";          objInt<<176588;objType<<"HERB";profLvl<<290;  // Icecap
objStr<<"Черный лотос";         objInt<<176589;objType<<"HERB";profLvl<<300;  // Black Lotus
objStr<<"Кровопийка";           objInt<<181166;objType<<"HERB";profLvl<<1;  // Bloodthistle
objStr<<"Сквернопля";           objInt<<181270;objType<<"HERB";profLvl<<300;  // Felweed
objStr<<"Сияние грез";          objInt<<181271;objType<<"HERB";profLvl<<315;  // Dreaming Glory
objStr<<"Кисейница";            objInt<<181275;objType<<"HERB";profLvl<<325;  // Ragveil
objStr<<"Огненный зев";         objInt<<181276;objType<<"HERB";profLvl<<335;  // Flame Cap
objStr<<"Терошишка";            objInt<<181277;objType<<"HERB";profLvl<<325;  // Terocone
objStr<<"Древний лишайник";     objInt<<181278;objType<<"HERB";profLvl<<340;  // Ancient Lichen
objStr<<"Пустоцвет";            objInt<<181279;objType<<"HERB";profLvl<<350;  // Netherbloom
objStr<<"Ползучий кошмарник";   objInt<<181280;objType<<"HERB";profLvl<<365;  // Nightmare Vine
objStr<<"Манаполох";            objInt<<181281;objType<<"HERB";profLvl<<375;  // Mana Thistle
objStr<<"Куст пустопраха";      objInt<<185881;objType<<"HERB";profLvl<<350;  // Netherdust Bush
objStr<<"Золотой клевер";       objInt<<189973;objType<<"HERB";profLvl<<350;  // Goldclover
objStr<<"Тигровая лилия";       objInt<<190169;objType<<"HERB";profLvl<<375;  // Tiger Lily
objStr<<"Роза Таландры";        objInt<<190170;objType<<"HERB";profLvl<<385;  // Talandra's Rose
objStr<<"Личецвет";             objInt<<190171;objType<<"HERB";profLvl<<425;  // Lichbloom
objStr<<"Ледошип";              objInt<<190172;objType<<"HERB";profLvl<<435;  // Icethorn
objStr<<"Мерзлая трава";        objInt<<190173;objType<<"HERB";profLvl<<415;  // Frozen Herb
objStr<<"Мерзлая трава";        objInt<<190174;objType<<"HERB";profLvl<<415;  // Frozen Herb
objStr<<"Мерзлая трава";        objInt<<190175;objType<<"HERB";profLvl<<415;  // Frozen Herb
objStr<<"Северный лотос";       objInt<<190176;objType<<"HERB";profLvl<<450;  // Frost Lotus
objStr<<"Язык аспида";          objInt<<191019;objType<<"HERB";profLvl<<400;  // Adder's Tongue
objStr<<"Огница";               objInt<<191303;objType<<"HERB";profLvl<<360;  // Firethorn

/*objStr<<"Потайной сейф";objInt<<2039;objType<<"OPEN"; // Hidden Strongbox
objStr<<"Гигантский моллюск";objInt<<2744;objType<<"OPEN"; // Giant Clam
objStr<<"Подбитый сундук";objInt<<2843;objType<<"OPEN"; // Battered Chest
objStr<<"Побитый сундук";objInt<<2844;objType<<"OPEN"; // Tattered Chest
objStr<<"Добротный сундук";objInt<<2850;objType<<"OPEN"; // Solid Chest
objStr<<"Бочка с водой";objInt<<3658;objType<<"OPEN"; // Water Barrel
objStr<<"Бочка дынного сока";objInt<<3659;objType<<"OPEN"; // Barrel of Melon Juice
objStr<<"Оружейный ящик";objInt<<3660;objType<<"OPEN"; // Armor Crate
objStr<<"Ящик с оружием";objInt<<3661;objType<<"OPEN"; // Weapon Crate
objStr<<"Ящик со съестными припасами";objInt<<3662;objType<<"OPEN"; // Food Crate
objStr<<"Бочка молока";objInt<<3705;objType<<"OPEN"; // Barrel of Milk
objStr<<"Бочка сладкого нектара";objInt<<3706;objType<<"OPEN"; // Barrel of Sweet Nectar
objStr<<"Сейф Альянса";objInt<<3714;objType<<"OPEN"; // Alliance Strongbox
objStr<<"Ящик с различными деталями";objInt<<19019;objType<<"OPEN"; // Box of Assorted Parts
objStr<<"Разбитый ящик";objInt<<28604;objType<<"OPEN"; // Scattered Crate
objStr<<"Большой сундук; окованный железом";objInt<<74447;objType<<"OPEN"; // Large Iron Bound Chest
objStr<<"Большой добротный сундук";objInt<<74448;objType<<"OPEN"; // Large Solid Chest
objStr<<"Большой побитый сундук";objInt<<75293;objType<<"OPEN"; // Large Battered Chest
objStr<<"Сейф буканьера";objInt<<123330;objType<<"OPEN"; // Buccaneer's Strongbox
objStr<<"Большой сундук; окованный мифрилом";objInt<<131978;objType<<"OPEN"; // Large Mithril Bound Chest
objStr<<"Ящик Орды с припасами";objInt<<142191;objType<<"OPEN"; // Horde Supply Crate
objStr<<"Куча земли Ун'Горо";objInt<<157936;objType<<"OPEN"; // Un'Goro Dirt Pile
objStr<<"Синий Кристалл Силы";objInt<<164658;objType<<"OPEN"; // Blue Power Crystal
objStr<<"Зеленый кристалл Силы";objInt<<164659;objType<<"OPEN"; // Green Power Crystal
objStr<<"Красный Кристалл Силы";objInt<<164660;objType<<"OPEN"; // Red Power Crystal
objStr<<"Желтый Кристалл Силы";objInt<<164661;objType<<"OPEN"; // Yellow Power Crystal
objStr<<"Очищенный Ночной дракон";objInt<<164881;objType<<"OPEN"; // Cleansed Night Dragon
objStr<<"Очищенный песнецвет";objInt<<164882;objType<<"OPEN"; // Cleansed Songflower
objStr<<"Очищенный ветроцвет";objInt<<164884;objType<<"OPEN"; // Cleansed Windblossom
objStr<<"Побег кровоцвета";objInt<<164958;objType<<"OPEN"; // Bloodpetal Sprout
objStr<<"Очищенный кнутокорень";objInt<<174622;objType<<"OPEN"; // Cleansed Whipper Root
objStr<<"Кровь героев";objInt<<176213;objType<<"OPEN"; // Blood of Heroes
objStr<<"Ловушка на моллюска";objInt<<176582;objType<<"OPEN"; // Shellfish Trap
objStr<<"Учебный сейф";objInt<<178244;objType<<"OPEN"; // Practice Lockbox
objStr<<"Побитый сундучок";objInt<<179486;objType<<"OPEN"; // Battered Footlocker
objStr<<"Затопленный сундучок";objInt<<179487;objType<<"OPEN"; // Waterlogged Footlocker
objStr<<"Проломленный сундучок";objInt<<179492;objType<<"OPEN"; // Dented Footlocker
objStr<<"Замшелый сундучок";objInt<<179493;objType<<"OPEN"; // Mossy Footlocker
objStr<<"Сундучок Алых";objInt<<179498;objType<<"OPEN"; // Scarlet Footlocker
objStr<<"Погребальный сундук";objInt<<181665;objType<<"OPEN"; // Burial Chest
objStr<<"Сундук из оскверненного железа";objInt<<181798;objType<<"OPEN"; // Fel Iron Chest
objStr<<"Тяжелый сундук из оскверненного железа";objInt<<181800;objType<<"OPEN"; // Heavy Fel Iron Chest
objStr<<"Сундук; окованный адамантитом";objInt<<181802;objType<<"OPEN"; // Adamantite Bound Chest
objStr<<"Сундук из оскверненной стали";objInt<<181804;objType<<"OPEN"; // Felsteel Chest
objStr<<"Огнешляпка";objInt<<182053;objType<<"OPEN"; // Glowcap
objStr<<"Плетеный ларец";objInt<<184740;objType<<"OPEN"; // Wicker Chest
objStr<<"Примитивный сундук";objInt<<184793;objType<<"OPEN"; // Primitive Chest
objStr<<"Добротный сундук из оскверненного железа";objInt<<184930;objType<<"OPEN"; // Solid Fel Iron Chest
objStr<<"Яйцо дракона из стаи Крыльев Пустоты";objInt<<185915;objType<<"OPEN"; // Netherwing Egg
objStr<<"Обломок вечной мерзлоты";objInt<<193997;objType<<"OPEN"; // Everfrost Chip*/
}

static int WaitMsLoadNode = 1200;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    version = new QLabel(this);



    version->setText("Версия за 26.10.2019");



    ui->statusBar->addWidget(version);

    new QShortcut(QKeySequence(Qt::Key_F1), this, SLOT(stopFarmF1()));
    new QShortcut(QKeySequence(Qt::Key_F2), this, SLOT(on_pushButtonXY_clicked()));

    model = new QSqlQueryModel(this);
    LocalPlayer = new WowObject();
    LocalTarget = new WowObject();
    CurrentObject = new WowObject();
    TempObject = new WowObject();
    Timer=new QTimer();
    TimerReloadAdreses=new QTimer();
    TimerIsLoading=new QTimer();
    connect(TimerIsLoading, SIGNAL(timeout()), this, SLOT(IsLoadingOrConnecting()));
    connect(Timer, SIGNAL(timeout()), this, SLOT(TimerOut()));
    connect(TimerReloadAdreses, SIGNAL(timeout()), this, SLOT(TimerReloadAdresesOut()));
    connect(&TimerJaine, SIGNAL(timeout()), this, SLOT(TimerJaineOut()));
    connect(&TimerFindBots, SIGNAL(timeout()), this, SLOT(TimerFindBotsOut()));

//    TimerIsLoading->start(200);
    TimerReloadAdreses->start(500);

    ui->tableView->scrollToTop();
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
//    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

//    ui->tableView->setVisible(false);
//    ui->tableView->resizeColumnToContents(0);
//    ui->tableView->setVisible(true);

    ui->tableView->hide();

    ui->labelLooted->hide();

    ui->tableWidget_2->scrollToTop();
    ui->tableWidget_2->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->tableWidget_2->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tableWidget_2->setColumnHidden(2,true);
    ui->tableWidget_2->hide();

    ui->frame_2->hide();

    ui->groupBox_6->setTitle("Список");
    ui->StartButton->setIcon(QPixmap(":/png/32x32/play.png"));

//    QFile DB("DB.db");
//    DB.remove();

    ui->ZposToMacros->setValue(70);
    ui->WileStepBox->setValue(TimerMS);

    db = QSqlDatabase::addDatabase("QSQLITE","connect1");
    db.setDatabaseName("Data.db");
    db.open();
    query  = new QSqlQuery(db);
    secondQuery  = new QSqlQuery(db);

    ctr  = "PRAGMA synchronous = OFF";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    ctr  = "PRAGMA journal_mode = MEMORY";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }

    //создаем таблицы//
    ctr  = "DROP TABLE IF EXISTS NpcData";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    ctr  = "CREATE TABLE IF NOT EXISTS NpcData ("
                "`MapName`      TEXT,"
                "`Name`      TEXT,"
                "`ID`        TEXT,"
                   "`X`      TEXT,"
                   "`Y`      TEXT,"
                   "`Z`      TEXT)";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }

    //создаем таблицы//
    ctr  = "DROP TABLE IF EXISTS PlayerData";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    ctr  = "CREATE TABLE IF NOT EXISTS PlayerData ("
                "`MapName`      TEXT,"
                "`Name`      TEXT,"
                "`ID`        TEXT,"
                   "`X`      TEXT,"
                   "`Y`      TEXT,"
                   "`Z`      TEXT)";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }

// !!!!!!!!  ctr  = "DROP TABLE IF EXISTS NodeData";
// !!!!!!!   if(!query->exec(ctr)){
// !!!!!!!     qDebug() << query->lastError();
//  !!!!!      qDebug() << ctr;
//  !!!!!!!  }
    ctr  = "CREATE TABLE IF NOT EXISTS NodeData ("
           "`ID`	INTEGER PRIMARY KEY AUTOINCREMENT,"
           "`Continent`    TEXT,"
           "`MapName`      TEXT,"
           "`NodeType`     TEXT,"
           "`NodeName`     TEXT,"
           "`X`     TEXT,"
           "`Y`     TEXT,"
           "`Z`     TEXT,"
           "`LastUse`     TEXT,"
           "`XY`	INTEGER)";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }

//        ctr  = "DROP TABLE IF EXISTS NodeFiltr";
//        if(!query->exec(ctr)){
//            qDebug() << query->lastError();
//            qDebug() << ctr;
//        }
    ctr  = "CREATE TABLE IF NOT EXISTS NodeFiltr ("
                "`Name`     TEXT,"
                "`LvL`      integer,"
                "`State`    integer,"
                "`Type`     TEXT)"
            ;
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }


    ctr  = "SELECT State FROM NodeFiltr";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    int i=0;
    while(query->next()){
        i++;
    }
    if(i!=objInt.count()){
        for(i=0;i<objInt.count();i++){
            ctr = "insert into nodefiltr (Name,LvL,State,Type) VALUES ('%1','%2','%3','%4')";
            ctr = ctr.arg(objStr[i]).arg(profLvl[i]).arg(1).arg(objType[i]);
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
        }
    }

    ctr  = "SELECT UserName, Password FROM Settings";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    while(query->next()){
        UserName = query->value(0).toString();
        Password = query->value(1).toString();
    }

//    if(!query->exec("update NodeData set loottime = NULL")){
//        qDebug() << query->lastError();
//        qDebug() << ctr;
//    }


    ui->actionSetFilter->setDisabled(true);
    ui->actionZeroNodes->setDisabled(true);

    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");   //для чтения кириллицы
    QSettings settings("settings.conf", QSettings::IniFormat);
    settings.setIniCodec(codec);
    WaitMsLoadNode=settings.value("Macros/WaitMsLoadNode").toInt();
    ui->lineWaitMsLoadNode->setText(QString::number(WaitMsLoadNode));

//    initEnum();
    LoadAddresses();
//    updateFiltrTable();

    this->show();
    TimerJaineOut();
}

void MainWindow::updateFiltrTable(){
    if(TableViewMode=="AND NodeType IN('MINE')"){
        ctr  = "SELECT * FROM NodeFiltr WHERE Type = 'MINE' Order by LvL";
    }
    else if(TableViewMode=="AND NodeType IN('HERB')"){
        ctr  = "SELECT * FROM NodeFiltr WHERE Type = 'HERB' Order by LvL";
    }
    else{
        ctr  = "SELECT * FROM NodeFiltr Order by LvL";
    }

    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    QStringList Labels;
    Labels<<"Имя"<<"Навык"<<"State";
    int s=0;
    int ColumnCount=3;
    ui->tableWidget_2->clear();
    ui->tableWidget_2->setRowCount(0);
    ui->tableWidget_2->setColumnCount(ColumnCount);
    while(query->next()){
        ui->tableWidget_2->setRowCount(ui->tableWidget_2->rowCount()+1);
        for (int j = 0; j < ColumnCount; ++j) {
            delete  ui->tableWidget_2->item(s, j);
            ui->tableWidget_2->setItem(s, j, new QTableWidgetItem(query->value(j).toString()));
            ui->tableWidget_2->item(s,j)->setTextAlignment(Qt::AlignCenter);
            if(query->value(2).toInt()==1)
                ui->tableWidget_2->item(s,j)->setBackground(QColor(0,255,0,100));
//            qDebug()<<query->value(j).toString();
        }
        s++;
    }
    ui->tableWidget_2->setHorizontalHeaderLabels(Labels);
}

void delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}


void FlyOn(){
    DWORD flyON = 16777216;
    WriteProcessMemory(hProc, reinterpret_cast<void*>(RM(RM(RM(RM(RM(0x400000+0x8D87A8)+0x34)+0x24)+0x1C)+0x4)+0x7CC), &flyON, sizeof(flyON), nullptr);

}
void FlyOFF(){
    DWORD flyOFF = 2147483648;
    WriteProcessMemory(hProc, reinterpret_cast<void*>(RM(RM(RM(RM(RM(0x400000+0x8D87A8)+0x34)+0x24)+0x1C)+0x4)+0x7CC), &flyOFF, sizeof(flyOFF), nullptr);

}

float MainWindow::RadianToDegree(float Rotation)
{
    return Rotation * (180 / static_cast<float>(M_PI));
}

QString MainWindow::MobNameFromGuid(ulong Guid)
{
    uint ObjectBase = GetObjectBaseByGuid(Guid);
    return RMstring(RM(RM(ObjectBase + 0x964) + 0x05C));
}

QString MainWindow::PlayerNameFromGuid(ulong guid)
{
    ulong mask, base_, offset, current, shortGUID, testGUID;
    mask = RM(NameOffsets::nameStore + NameOffsets::nameMask);
    base_ = RM(NameOffsets::nameStore + NameOffsets::nameBase);
    shortGUID = guid & 0xffffffff;
    offset = 12 * (mask & shortGUID);
    current = RM(base_ + offset + 8);
    offset = RM(base_ + offset);
    if ((current & 0x1) == 0x1) {
        return "";
    }
    testGUID = RM(current);
    while (testGUID != shortGUID){
        current = RM(current + offset + 4);
        if ((current & 0x1) == 0x1) {
            return "";
        }
        testGUID = RM(current);
    }
    return RMstring(current + NameOffsets::nameString);
}

uint MainWindow::GetObjectBaseByGuid(ulong Guid)
{
    TempObject->BaseAddress = FirstObject;
    while (TempObject->BaseAddress != 0 && (TempObject->BaseAddress & 1) == 0)
    {
        TempObject->Guid = RM(TempObject->BaseAddress + 0x30);
        if (TempObject->Guid == Guid)
            return TempObject->BaseAddress;

        TempObject->BaseAddress = RMuint(TempObject->BaseAddress + 0x3C);

//        TempObject->Guid = RM(TempObject->BaseAddress + ObjectOffsets::Guid);
//        if (TempObject->Guid == Guid)
//            return TempObject->BaseAddress;
//        TempObject->BaseAddress = RM(TempObject->BaseAddress + ClientOffsets::NextObjectOffset);
    }
    return 0;
}

ulong MainWindow::GetObjectGuidByBase(uint Base)
{
    return RM(Base + ObjectOffsets::Guid);
}

QString ToHexString(uint HexToConvert)
{
    return "0x" + QString::number(HexToConvert);
}


#include <iostream>
#include <tlhelp32.h>
#include <tchar.h>
#include <psapi.h>

void MainWindow::TeleportTo(float X, float Y, float Z){
//    qDebug()<<X<<Y<<Z;
    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharX), &X, sizeof(X), nullptr);
    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharY), &Y, sizeof(Y), nullptr);
    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharZ), &Z, sizeof(Z), nullptr);
}
void MainWindow::TeleportToD(double Xd, double Yd, double Zd){
//    qDebug()<<X<<Y<<Z;
    float X = static_cast<float>(Xd);
    float Y = static_cast<float>(Yd);
    float Z = static_cast<float>(Zd);
    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharX), &X, sizeof(X), nullptr);
    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharY), &Y, sizeof(Y), nullptr);
    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharZ), &Z, sizeof(Z), nullptr);
}


void MainWindow::TeleportToDeathPoint(){
    float X = RMfloat(0x00BD0A5C);
    float Y = RMfloat(0x00BD0A58);
    float Z = RMfloat(0x00BD0A60);
//    qDebug()<<X<<Y<<Z;
    TeleportTo(X,Y,Z);
}

#define MIN(x, y) ((x) > (y)) ? (y) : (x)
void cstringToTCHAR(TCHAR *dst, const char *src, size_t l) {
#if defined(_UNICODE) || defined(UNICODE)
    mbstowcs(dst, src, l);
#else
    memcpy(dst, src, l);
#endif
}

DWORD FindProcessId(char* processName) {
    char* p = strrchr(processName, '\\');

    if(p) {
        processName = p+1;
    }

    PROCESSENTRY32 processInfo;
    processInfo.dwSize = sizeof(processInfo);

    HANDLE processesSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    if (processesSnapshot == INVALID_HANDLE_VALUE) {
        return 0;
    }

    TCHAR systemEncodeProcessName[30];
    size_t processNameLen = MIN((strlen(processName) + 1), 30);
    cstringToTCHAR(systemEncodeProcessName, processName, processNameLen);

    Process32First(processesSnapshot, &processInfo);

    if (!_tcscmp(systemEncodeProcessName, processInfo.szExeFile)) {
        CloseHandle(processesSnapshot);
        return processInfo.th32ProcessID;
    }
    while ( Process32Next(processesSnapshot, &processInfo) ) {
        if ( !_tcscmp(systemEncodeProcessName, processInfo.szExeFile) ) {
            CloseHandle(processesSnapshot);
            return processInfo.th32ProcessID;
        }
    }
    CloseHandle(processesSnapshot);
    return 0;
}

QVector<int> MainWindow::GetBuffs(DWORD curObj){
    QVector<int> Buffs;

    DWORD auraTable = curObj + UnitBaseGetUnitAura::AURA_TABLE_1;
    int auraCount = RMint(curObj + UnitBaseGetUnitAura::AURA_COUNT_1);


//    qDebug() << "auraTable1" << reinterpret_cast<LPCVOID>(auraTable) << auraCount;
    if(auraCount == -1){
        auraTable = curObj + UnitBaseGetUnitAura::AURA_TABLE_2;
        auraCount = RMint(curObj + UnitBaseGetUnitAura::AURA_COUNT_2);
    }
//    qDebug() << "auraTable2" << reinterpret_cast<LPCVOID>(auraTable) << auraCount;

//    auraTable = curObj - 0x390520;

    for(int i = 0;i<auraCount;i++){
        int spellId = RMint(auraTable + UnitBaseGetUnitAura::AURA_SIZE * static_cast<uint>(i) + UnitBaseGetUnitAura::AURA_SPELL_ID);
//        int spellId = RMint(auraTable + UnitBaseGetUnitAura::AURA_SIZE * static_cast<uint>(i));
//        qDebug() << reinterpret_cast<LPCVOID>(auraTable + UnitBaseGetUnitAura::AURA_SIZE * static_cast<uint>(i) + UnitBaseGetUnitAura::AURA_SPELL_ID) << spellId;

        if(spellId!=0)
            Buffs<<spellId;
    }
    return Buffs;
}

bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType)
    Q_UNUSED(result)
    // Transform the message pointer to the MSG WinAPI
    MSG* msg = reinterpret_cast<MSG*>(message);

    // If the message is a HotKey, then ...
    if(msg->message == WM_HOTKEY){
        // ... check HotKey
        if(msg->wParam == 100){
            // We inform about this to the console
//            qDebug() << "HotKey worked";
            if(StartMode==1)
                stopFarmF1();
            else
                on_StartButton_clicked();
            return true;
        }
    }
    return false;
}

static DWORD PID, macros_addr, last_chat_msg_addr;

bool MainWindow::LoadAddresses()
{
//    while(!IsLoadingOrConnecting()==true){
//        delay(200);
//    }
    hCurWnd = nullptr;
    QString proccesName = "wow.exe";
    char *procName=static_cast<char*>(malloc(10));
    QByteArray ba=proccesName.toLatin1();
    strcpy(procName,ba.data());

    PID = FindProcessId(procName);
    hCurWnd = FindWindow(nullptr, _T("World of Warcraft"));
    RegisterHotKey(reinterpret_cast<HWND>(MainWindow::winId()),   // Set the system identifier of the widget window that will handle the HotKey
                   100,                         // Set identifier HotKey
                   0,         // Set modifiers
                   0x70);                        // We define hotkeys for HotKey

    hProc = OpenProcess(PROCESS_VM_READ|PROCESS_VM_OPERATION|PROCESS_QUERY_INFORMATION|PROCESS_VM_WRITE,false,PID);
    if(hProc==nullptr){
        QString proccesName = "run.exe";
        char *procName=static_cast<char*>(malloc(10));
        QByteArray ba=proccesName.toLatin1();
        strcpy(procName,ba.data());
        PID = FindProcessId(procName);
        hProc = OpenProcess(PROCESS_VM_READ|PROCESS_VM_OPERATION|PROCESS_QUERY_INFORMATION|PROCESS_VM_WRITE,false,PID);
    }

    ClientConnection = RM(ClientOffsets::StaticClientConnection);
    ObjectManager = RM(ClientConnection + ClientOffsets::ObjectManagerOffset);
    FirstObject = RM(ObjectManager + ClientOffsets::FirstObjectOffset);
    LocalTarget->Guid = RM(ClientOffsets::LocalPlayerGUID);
    LocalPlayer->Guid = RM(ObjectManager + ClientOffsets::LocalGuidOffset);

    adresCharX=RM(RM(RM(RM(RM(0x400000+0x8D87A8)+0x34)+0x24)+0x1C)+0x4)+0x79C;
    adresCharY=RM(RM(RM(RM(RM(0x400000+0x8D87A8)+0x34)+0x24)+0x1C)+0x4)+0x798;
    adresCharZ=RM(RM(RM(RM(RM(0x400000+0x8D87A8)+0x34)+0x24)+0x1C)+0x4)+0x7A0;
    adresCharOX=RM(RM(RM(RM(RM(0x400000+0x8D87A8)+0x34)+0x24)+0x1C)+0x4)+0x7A8;

    LocalPlayer->BaseAddress = GetObjectBaseByGuid(LocalPlayer->Guid);
    LocalPlayer->UnitFieldsAddress = RM(LocalPlayer->BaseAddress + ObjectOffsets::UnitFields);

    macros_addr = RM(RM(RM(RM(RM(RM(0x400000+0x7EAF84)+0x4)+0xC)+0x10)+0x4)+0x0)+0x15C;
    qDebug()<<"macros_addr"<<reinterpret_cast<LPCVOID>(macros_addr);
    last_chat_msg_addr = RM(RM(RM(RM(RM(RM(0x400000+0x9CE674)+0x0)+0x98)+0x54)+0x8)+0xD4)+0x20;
    qDebug()<<"last_chat_msg_addr"<<reinterpret_cast<LPCVOID>(last_chat_msg_addr);


//        qDebug()<<"LocalPlayer->BaseAddress"<<reinterpret_cast<LPCVOID>(LocalPlayer->BaseAddress);
//        qDebug()<<"LocalPlayer->UnitFieldsAddress"<<reinterpret_cast<LPCVOID>(LocalPlayer->UnitFieldsAddress);

//        if(reinterpret_cast<LPCVOID>(LocalPlayer->BaseAddress)==nullptr){
//            delay(1000);
//            ui->textEdit->append("Zайдите персом в мир)");
//        }
//        else{
//            ui->textEdit->append("Читер вошел в игровой мир");
//        }


    //    qDebug()<<"Client Connection: " + ToHexString(ClientConnection);
    //    qDebug()<<"Object Manager: " + ToHexString(ObjectManager);
    //    qDebug()<<"First Object: " + ToHexString(FirstObject);

        if(reinterpret_cast<LPCVOID>(LocalPlayer->BaseAddress)==nullptr){
//            RadarIsReady = false;
//              qDebug()<<"RadarIsReady = false";
//              this->setDisabled(true);
            return false;
        }
        else{
//            RadarIsReady = true;
//            qDebug()<<"RadarIsReady = true";
//            this->setEnabled(true);
            return true;
        }
}


void SendKey(WPARAM key)
{
    PostMessage(hCurWnd, WM_KEYDOWN, key, 0);
    delay(10);
    PostMessage(hCurWnd, WM_KEYUP, key, 0);
}

QString LocaWoW_TO_LocRadar(QString LocWoW){
    QString MapName;
    if(LocWoW=="Борейская тундра")
        MapName="3_Борейская тундра";
    else if(LocWoW=="Низина Шолазар")
        MapName="4_Низина Шолазар";
    else if(LocWoW=="Ледяная Корона")
        MapName="6_Ледяная Корона";
    else if(LocWoW=="Озеро Ледяных Оков")
        MapName="5_Озеро Ледяных Оков";
    else if(LocWoW=="Драконий Погост")
        MapName="2_Драконий Погост";
    else if(LocWoW=="Лес Хрустальной Песни")
        MapName="1_Лес Хрустальной Песни";
    else if(LocWoW=="Грозовая Гряда")
        MapName="7_Грозовая Гряда";
    else if(LocWoW=="Зул'Драк")
        MapName="8_Зул'Драк";
    else if(LocWoW=="Седые холмы")
        MapName="9_Седые холмы";
    else if(LocWoW=="Ревущий фьорд")
        MapName="A_Ревущий фьорд";

    else if(LocWoW=="Лес Тероккар")
        MapName="3_Лес Тероккар";
    else if(LocWoW=="Награнд")
        MapName="4_Награнд";
    else if(LocWoW=="Зангартопь")
        MapName="5_Зангартопь";
    else if(LocWoW=="Острогорье")
        MapName="6_Острогорье";
    else if(LocWoW=="Пустоверть")
        MapName="7_Пустоверть";
    else if(LocWoW=="Полуостров Адского Пламени")
        MapName="8_Полуостров Адского Пламени";
    else if(LocWoW=="Долина Призрачной Луны")
        MapName="1_Долина Призрачной Луны";
    else
        MapName = LocWoW;
    return MapName;
}

static int ToMacros = 0, ZOffsetTP = -900;
static float ZposToMacros = 70;
//static ushort MyKey;
static QVector<int> NodesInLocation, NodeToPrio4, NodeStopRound2;

static QTime prev;
void MainWindow::RadarTimer_Tick(QString Mode)
{
    if(ui->actionqDebug_RadarTimer_Tick_STEP->isChecked()){
        qDebug()<<QTime::currentTime().toString("hh:mm:ss.zzz")<<"RadarTimer_Tick -"<<prev.msecsTo(QTime::currentTime());
        prev=QTime::currentTime();
    }

//    CurContinent = RMint(ClientOffsets::CurrentContinent);
//    qDebug()<<"CurContinent"<<CurContinent;
    //    for(int i=0;i<objInt.count();i++)
    //        qDebug()<<objInt[i]<<objStr[i]<<objType[i];


//    if(QTime::currentTime().hour()==0 && QTime::currentTime().minute()==0 && (QTime::currentTime().second()>0 || QTime::currentTime().second()<5))
//        on_actionZeroNodes_triggered();

//    if(StartMode==0){
//        ui->textEdit->clear();
//        qDebug()<<"\n";
//    }

    TotalWowObjects = 0;
    TotalNpc = 0;
    TotalPlayer = 0;
    TotalNode = 0;
    CurrentObject->BaseAddress = FirstObject;
    LocalPlayer->BaseAddress = GetObjectBaseByGuid(LocalPlayer->Guid);
    LocalPlayer->XPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_X);
    LocalPlayer->YPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_Y);
    LocalPlayer->ZPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_Z);
    LocalPlayer->UnitFieldsAddress = RM(LocalPlayer->BaseAddress + ObjectOffsets::UnitFields);
    LocalPlayer->Name = PlayerNameFromGuid(LocalPlayer->Guid);
    LocalTarget->Guid = RM(ClientOffsets::LocalTargetGUID);
    LocalPlayer->Continent = RMstring(0xCE06D0);
    LocalPlayer->CurrentHealth = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
    LocalPlayer->MaxHealth = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth);
    LocalPlayer->Faction = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Faction);

//    qDebug()<<LocalPlayer->Faction;


//    LocalPlayer->CurrentEnergy = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Energy);
//    LocalPlayer->MaxEnergy = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxEnergy);
//    LocalPlayer->Level = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Level);
//    LocalPlayer->Rotation = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Rot);


//    if (LocalTarget->Guid != 0)
//    {
//        LocalTarget->BaseAddress = GetObjectBaseByGuid(LocalTarget->Guid);
//        LocalTarget->buffs = GetBuffs(LocalTarget->BaseAddress);
//        for (int i=0;i<LocalTarget->buffs.count();i++) {
//            qDebug()<<LocalTarget->buffs[i];

//        }
//        LocalTarget->XPos = RMfloat(LocalTarget->BaseAddress + ObjectOffsets::Pos_X);
//        LocalTarget->YPos = RMfloat(LocalTarget->BaseAddress + ObjectOffsets::Pos_Y);
//        LocalTarget->ZPos = RMfloat(LocalTarget->BaseAddress + ObjectOffsets::Pos_Z);
//        LocalTarget->Type = RMuint(LocalTarget->BaseAddress + ObjectOffsets::Type);
//        LocalTarget->Rotation = RMfloat(LocalTarget->BaseAddress + ObjectOffsets::Rot);
//        LocalTarget->UnitFieldsAddress = RM(LocalTarget->BaseAddress + ObjectOffsets::UnitFields);
//        LocalTarget->CurrentHealth = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::Health);
//        LocalTarget->MaxHealth = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::MaxHealth);
//        LocalTarget->CurrentEnergy = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::Energy);
//        LocalTarget->MaxEnergy = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::MaxEnergy);
//        LocalTarget->Level = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::Level);
//        LocalTarget->SummonedBy = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::SummonedBy);
//        LocalTarget->UnitID = RM(LocalTarget->UnitFieldsAddress + UnitOffsets::UnitID);

//        if (LocalTarget->Type == 3) {
//            // npc
//            LocalTarget->Name = MobNameFromGuid(LocalTarget->Guid);
//        }
//        if (LocalTarget->Type == 4)
//            // player
//            LocalTarget->Name = PlayerNameFromGuid(LocalTarget->Guid);
//    }

    ctr  = "DELETE FROM NpcData;";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }

    QStringList MapNames,NpcNames,NpcIDs;
    while (CurrentObject->BaseAddress != 0 && CurrentObject->BaseAddress % 2 == 0)
    {
        TotalWowObjects = TotalWowObjects + 1;

        CurrentObject->Guid = RM(CurrentObject->BaseAddress + ObjectOffsets::Guid);
        CurrentObject->Type = RM(CurrentObject->BaseAddress + ObjectOffsets::Type);
        CurrentObject->XPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Pos_X);
        CurrentObject->YPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Pos_Y);
        CurrentObject->ZPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Pos_Z);
        CurrentObject->Continent = RMstring(0xCE06D0);
        CurrentObject->MapName = RMstring(RM(0x00BD0788));
        //        CurrentObject->Rotation = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Rot);
        //        CurrentObject->Level = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::Level);

        if (CurrentObject->Type == 3){ // not a human player
            CurrentObject->Name = MobNameFromGuid(CurrentObject->Guid);
        }
        if (CurrentObject->Type == 4){ // a human player
            CurrentObject->Name = PlayerNameFromGuid(CurrentObject->Guid);
        }

        if(CurrentObject->Guid != LocalTarget->Guid){
             // a npc
            if(CurrentObject->Type==3 && (Mode!="Player+Node" || Mode!="Node")){
                CurrentObject->UnitFieldsAddress = RM(CurrentObject->BaseAddress + ObjectOffsets::UnitFields);
                CurrentObject->UnitID = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::UnitID);
                CurrentObject->CurrentHealth = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::Health);
                CurrentObject->MaxHealth = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::MaxHealth);
                //CurrentObject->SummonedBy = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::SummonedBy);
                //CurrentObject->CurrentEnergy = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::Energy);
                //CurrentObject->MaxEnergy = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::MaxEnergy);
                if(Mode=="Npc"){
//                    qDebug()<<Mode<< CurrentObject->Name <<"NpcID"<<CurrentObject->UnitID<<"X"<<CurrentObject->XPos<<"Y"<<CurrentObject->YPos<<"Z"<<CurrentObject->ZPos;



                    QString MapName1=CurrentObject->MapName;
                    MapName1=MapName1.replace("'","''");

                    QString NpcName1=CurrentObject->Name;
                    NpcName1=NpcName1.replace("'","''");

//                    ctr  = "DELETE FROM NpcData WHERE MapName='%1' AND Name='%2' AND ID='%3' AND X='%4' AND Y='%5' AND Z='%6';";
//                    ctr = ctr.arg(MapName1)
//                            .arg(NpcName1)
//                            .arg(QString::number(CurrentObject->UnitID))
//                            .arg(static_cast<double>(CurrentObject->XPos))
//                            .arg(static_cast<double>(CurrentObject->YPos))
//                            .arg(static_cast<double>(CurrentObject->ZPos))
//                            ;
                    ctr  = "INSERT INTO NpcData (MapName,Name,ID,X,Y,Z) VALUES ('%1','%2','%3','%4','%5','%6');";
                    ctr = ctr.arg(MapName1)
                            .arg(NpcName1)
                            .arg(QString::number(CurrentObject->UnitID))
                            .arg(QString::number(static_cast<double>(CurrentObject->XPos)))
                            .arg(QString::number(static_cast<double>(CurrentObject->YPos)))
                            .arg(QString::number(static_cast<double>(CurrentObject->ZPos)));
//                    qDebug()<<ctr;
                    if(!query->exec(ctr)){
                        qDebug() << query->lastError();
                        qDebug() << ctr;
                    }
                }
                TotalNpc = TotalNpc + 1;
            }
            // a player
            else if(CurrentObject->Type==4){
                CurrentObject->UnitFieldsAddress = RM(CurrentObject->BaseAddress + ObjectOffsets::UnitFields);
                CurrentObject->UnitID = RM(CurrentObject->UnitFieldsAddress - UnitOffsets::PlayerID);
                //                CurrentObject->CurrentHealth = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::Health);
                //                CurrentObject->MaxHealth = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::MaxHealth);
                //                CurrentObject->MaxEnergy = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::MaxEnergy);
                //                CurrentObject->CurrentEnergy = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::Energy);
                //                CurrentObject->MaxEnergy = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::MaxEnergy);


                if(Mode=="Player+Node"){
//                    qDebug()<<Mode<< CurrentObject->Name <<"PlayerID"<<CurrentObject->UnitID<<"X"<<CurrentObject->XPos<<"Y"<<CurrentObject->YPos<<"Z"<<CurrentObject->ZPos;
                    if(CurrentObject->Name!=LocalPlayer->Name && PlayerInfo!=0){
                        QStringList str;
                        double x1 = static_cast<double>(LocalPlayer->XPos);
                        double y1 = static_cast<double>(LocalPlayer->YPos);
                        double z1 = 0.0, z12 = 0.0;
                        if(PlayerInfo==2){
                            z1 = static_cast<double>(LocalPlayer->ZPos-ZOffsetTP);
                            z12 = static_cast<double>(LocalPlayer->ZPos);
                        }
                        if(PlayerInfo==1){
                            z1 = z12 = static_cast<double>(LocalPlayer->ZPos);
                        }
                        double x2 = static_cast<double>(CurrentObject->XPos);
                        double y2 = static_cast<double>(CurrentObject->YPos);
                        double z2 = static_cast<double>(CurrentObject->ZPos);
                        double a =x1-x2;
                        double b =y1-y2;
                        double c =qPow(a,2)+qPow(b,2); c=qSqrt(c);
                        a=z1-z2;
                        double ame=z12-z2;
                        b=c;
                        c=qPow(a,2)+qPow(b,2);
                        double cme =qPow(ame,2)+qPow(b,2);
                        c=qSqrt(c);
                        cme=qSqrt(cme);

                        QString distanceToPoint = QString::number(c, 'f', 0);
                        QString distanceToMe = QString::number(cme, 'f', 0);

                        if(PlayerInfo==2){
                            ui->textEdit->append(CurrentObject->Name+" был в "+distanceToPoint+" метрах от точки и в "+distanceToMe+" от вас.");
                            qDebug()<<CurrentObject->Name+" был в "+distanceToPoint+" метрах от точки и в "+distanceToMe+" от вас.";
                        }
                        if(PlayerInfo==1){
                            ui->textEdit->append(CurrentObject->Name+" был в "+distanceToPoint+" метрах от вас.");
                            qDebug()<<CurrentObject->Name+" был в "+distanceToPoint+" метрах от вас.";
                        }


                        if(distanceToMe>1500){
                            qDebug()<<"x1"<<x1<<"y1"<<y1<<"z1"<<z1<<"z12"<<z12<<"x2"<<x2<<"y2"<<y2<<"z2"<<z2;
                            stopFarmF1();
                        }
//                        switch(level) {
//                            case ReportLevel::Info:
//                                mTeReports->setTextColor(Qt::blue);
//                                break;
//                            case ReportLevel::Warning:
//                                mTeReports->setTextColor(QColor::fromRgb(255, 165, 0)); // Orange
//                                break;
//                            case ReportLevel::Error:
//                                mTeReports->setTextColor(Qt::red);
//                                break;
//                        }

//                        // mTeReoports is just an instance of QTextEdit
//                        mTeReports->insertPlainText(tag + "\t");
//                        mTeReports->setTextColor(Qt::black); // set color back to black
//                        // might want ot use #ifdef for windows or linux....
//                        mTeReports->insertPlainText(report + "\r\n");

//                        // Force the scroll bar (if visible) to jump to bottom
//                        mTeReports->ensureCursorVisible();


//                        str<<CurrentObject->Name<<" был в "+distanceToPoint+" метрах от точки и в "+distanceToMe+" от вас.";

//                        QString strs;
//                        for(int l=0;l<str.count();l++){
//                            strs+=str[l];
//                        }
//                        ui->textEdit->append(strs);
                    }

                }

                if(Mode=="Player" && CurrentObject->Name!=LocalPlayer->Name){
//                        QStringList str;
//                        str<<CurrentObject->Name <<"   "<<QString::number(CurrentObject->UnitID)
//                          <<"   "<<"X"<<" "<<QString::number(static_cast<double>(CurrentObject->XPos)).replace(".",",")
//                         <<" "<<"Y"<<" "<<QString::number(static_cast<double>(CurrentObject->YPos)).replace(".",",")
//                        <<" "<<"Z"<<" "<<QString::number(static_cast<double>(CurrentObject->ZPos)).replace(".",",");
                    QString MapName1=CurrentObject->MapName;
                    MapName1=MapName1.replace("'","''");

                    QString Name1=CurrentObject->Name;
                    Name1=Name1.replace("'","''");

                        ctr  = "DELETE FROM PlayerData WHERE MapName='%1' AND Name='%2' AND ID='%3';";
                        ctr = ctr.arg(MapName1)
                                .arg(Name1)
                                .arg(QString::number(CurrentObject->UnitID));
//                        qDebug() << ctr;
                        if(!query->exec(ctr)){
                            qDebug() << query->lastError();
                            qDebug() << ctr;
                        }

                    ctr  = "INSERT INTO PlayerData (MapName,Name,ID,X,Y,Z) VALUES ('%1','%2','%3','%4','%5','%6');";
                    ctr = ctr.arg(MapName1)
                            .arg(Name1)
                            .arg(QString::number(CurrentObject->UnitID))
                            .arg(QString::number(static_cast<double>(CurrentObject->XPos)))
                            .arg(QString::number(static_cast<double>(CurrentObject->YPos)))
                            .arg(QString::number(static_cast<double>(CurrentObject->ZPos)));
//                        qDebug() << ctr;
                    if(!query->exec(ctr)){
                        qDebug() << query->lastError();
                        qDebug() << ctr;
                    }
                }
                if(CurrentObject->Name!=LocalPlayer->Name){
                    TotalPlayer = TotalPlayer + 1;
                }

                if(ToMacros==1 && CurrentObject->Name == "Macros"){
                    TeleportTo(CurrentObject->XPos,CurrentObject->YPos,CurrentObject->ZPos -ZposToMacros);
                    SendKey(0x53); //S key
                }
            }
            // a obj
            else if(CurrentObject->Type==5){

                CurrentObject->XPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Node_Pos_X);
                CurrentObject->YPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Node_Pos_Y);
                CurrentObject->ZPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Node_Pos_Z);
//                int displayID = RMint(RM(CurrentObject->BaseAddress + 0x08) + ObjectOffsets::DisplayID);
                int objectID = RMint(CurrentObject->BaseAddress + ObjectOffsets::ObjectID);

                CurrentObject->Name = "??";

                for(int o=0;o<objInt.count();o++)
                {
                    if (objectID == objInt[o])
                    {
                        CurrentObject->Name = objStr[o];
                        CurrentObject->objType = objType[o];
                    }
                }

//                if((Mode=="Node") && CurrentObject->Name != "??" /*&& CurrentObject->objType!="OPEN"*/){
//                    qDebug()<<CurrentObject->Name<<objectID;

                    //RESIZE MODEL NODE
//                    float SIZEMODEL = 5;
//                    DWORD adressSize = CurrentObject->BaseAddress+0x9C;
//                    qDebug()<<reinterpret_cast<LPCVOID>(adressSize)<<CurrentObject->Name;
//                    WriteProcessMemory(hProc, reinterpret_cast<void*>(adressSize), &SIZEMODEL, sizeof(SIZEMODEL), nullptr);
//                }


                //чек дистанции до объекта
//                if(CurrentObject->Name!= "??"){
//                    double x1 = static_cast<double>(LocalPlayer->XPos);
//                    double y1 = static_cast<double>(LocalPlayer->YPos);
//                    double x2 = static_cast<double>(CurrentObject->XPos);
//                    double y2 = static_cast<double>(CurrentObject->YPos);
//                    double a = x1 - x2;
//                    double b = y1 - y2;
//                    double c = qPow(a,2) + qPow(b,2);
//                    c = qSqrt(c);

//                    ui->textEdit->append("PlayerX - ObjX = "+QString::number(a));
//                    ui->textEdit->append("PlayerY - ObjY = "+QString::number(b));
//                    ui->textEdit->append(CurrentObject->Name+" distance = "+QString::number(c));
//                }

                if((Mode=="Node" || Mode=="Player+Node") && CurrentObject->Name != "??" && (CurrentObject->MapName != "Даларан" || CurrentObject->MapName != "Шаттрат")){

                    int ID = 0; QString LastUse;
                    //будут сохранятся ноды с другим именем но в той же точке, НЕ СТОИТ ЭТОГО ДЕЛАТЬ !!!!!!!!!!
//                            ctr  = "SELECT ID, LastUse FROM NodeData where Continent='%1' and NodeType='%2' and X='%3' and Y='%4' and Z='%5' and MapName = '%6' and NodeName='%7'";

                    ctr  = "SELECT ID, LastUse FROM NodeData where Continent='%1' and NodeType='%2' and X='%3' and Y='%4' and Z='%5'";
                    ctr = ctr
                            .arg(CurrentObject->Continent)
                            .arg(CurrentObject->objType)
                            .arg(QString::number(static_cast<double>(CurrentObject->XPos)))
                            .arg(QString::number(static_cast<double>(CurrentObject->YPos)))
                            .arg(QString::number(static_cast<double>(CurrentObject->ZPos)))
//                                    .arg(CurrentObject->MapName.replace("'","''"))
//                                    .arg(CurrentObject->Name.replace("'","''"))
                            ;
                    if(!query->exec(ctr)){
                        qDebug() << query->lastError();
                        qDebug() << ctr;
                    }
                    else{
                        while(query->next()){
                            ID = query->value(0).toInt();
                            LastUse = query->value(1).toString();
                        }
                    }
                    if(ID==0){

                        QString MapName1=CurrentObject->MapName;
                        MapName1=MapName1.replace("'","''");
                        MapName1=LocaWoW_TO_LocRadar(MapName1);

                        ctr  = "INSERT INTO NodeData (Continent,MapName,NodeType,NodeName,X,Y,Z,LastUse,XY) VALUES ('%7','%1','%2','%3','%4','%5','%6','0_NewNode',%8);";
                        ctr = ctr.arg(MapName1.replace("'","''"))
                                .arg(CurrentObject->objType)
                                .arg(CurrentObject->Name.replace("'","''"))
                                .arg(QString::number(static_cast<double>(CurrentObject->XPos)))
                                .arg(QString::number(static_cast<double>(CurrentObject->YPos)))
                                .arg(QString::number(static_cast<double>(CurrentObject->ZPos)))
                                .arg(CurrentObject->Continent)
                                .arg(0);
//                        qDebug() << ctr;
                        if(!query->exec(ctr)){
                            qDebug() << query->lastError();
                            qDebug() << ctr;
                        }
                        ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Add node "+ CurrentObject->Name);
                        qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Add node "<< CurrentObject->Name
                               <<"X"<<CurrentObject->XPos<<"Y"<<CurrentObject->YPos<<"Z"<<CurrentObject->ZPos;
                        NewNodesCounter++;
                    }
                    else if(Mode=="Player+Node"){
                        if((CurrentObject->objType==ui->tableView->model()->data(ui->tableView->model()->index(0,3)).toString())&&
                                (QString::number(static_cast<double>(CurrentObject->XPos))==ui->tableView->model()->data(ui->tableView->model()->index(0,5)).toString())&&
                                (QString::number(static_cast<double>(CurrentObject->YPos))==ui->tableView->model()->data(ui->tableView->model()->index(0,6)).toString())&&
                                (QString::number(static_cast<double>(CurrentObject->ZPos))==ui->tableView->model()->data(ui->tableView->model()->index(0,7)).toString())&&
                                (CurrentObject->Continent==ui->tableView->model()->data(ui->tableView->model()->index(0,1)).toString()))
                        {
                            if(NoNode==1){
                                qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Node Finded  "<<CurrentObject->Name.replace("'","''")
                                       <<ui->tableView->model()->data(ui->tableView->model()->index(0,0)).toString();
                                NoNode=0;
                            }
                        }
                        else {
                            if(ID!=ui->tableView->model()->data(ui->tableView->model()->index(0,0)).toInt() && LastUse=="4_ToReset"
                                    && setFiltrTableToSqlStr().contains(CurrentObject->Name)){
//                                if(!NodeToPrio4.contains(ID)){
//                                    NodeToPrio4<<ID;
//                                    qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Node Round_2  "<<CurrentObject->Name.replace("'","''")<<ID;
//                                }
                                if(!NodeStopRound2.contains(ID)){
                                    ctr = "UPDATE NodeData SET LastUse = '2_Round2' WHERE ID = '%1'";
                                    ctr=ctr.arg(ID);
                                  //qDebug() << ctr;
                                    if(!query->exec(ctr)){
                                        qDebug() << query->lastError();
                                        qDebug() << ctr;
                                    }
                                }
                            }
                            if(ID!=ui->tableView->model()->data(ui->tableView->model()->index(0,0)).toInt() && LastUse=="3_DeaultPrior"
                                    && setFiltrTableToSqlStr().contains(CurrentObject->Name)){
                                ctr = "UPDATE NodeData SET LastUse = '1_InLocation' WHERE ID = '%1'";
                                ctr=ctr.arg(ID);
//                                NodesInLocation<<ID;
//                                    qDebug() << ctr;
                                if(!query->exec(ctr)){
                                    qDebug() << query->lastError();
                                    qDebug() << ctr;
                                }

//                                qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Node InLocation "<<CurrentObject->Name.replace("'","''")<<ID;
                            }
//                            else{
//                                qDebug() << ID << "проебано скриптом";
//                            }
                        }
                    }
                    TotalNode = TotalNode + 1;
                }
            }
        }
//        qDebug()<<"Name"<<CurrentObject->Name<<"Type"<<CurrentObject->Type
//               <<static_cast<double>(CurrentObject->XPos)
//              <<static_cast<double>(CurrentObject->YPos)
//             <<static_cast<double>(CurrentObject->ZPos);


        CurrentObject->BaseAddress = RM(CurrentObject->BaseAddress + 0x3C);
    }

//    if(Mode=="Npc"){
//        qDebug()<<"---------------\nTotal Npc In Range: " << TotalNpc;
//        if(StartMode==0)
//            ui->textEdit->append("---------------\nTotal Npc In Range: " +QString::number(TotalNpc));
//    }
//    if(Mode=="Player"){
//        qDebug()<<"---------------\nTotal Player In Range: " << TotalPlayer;
//        if(StartMode==0)
//            ui->textEdit->append("---------------\nTotal Player In Range: " +QString::number(TotalPlayer));
//    }
//    if(Mode=="Node"){
//        qDebug()<<"---------------\nTotal Node In Range: " << TotalNode;
//        if(StartMode==0)
//            ui->textEdit->append("---------------\nTotal Node In Range: " +QString::number(TotalNode));
//    }
//    qDebug()<<"Total Objects In Manager: " << TotalWowObjects;
//    if(StartMode==0)
//        ui->textEdit->append("Total Objects In Manager: " +QString::number(TotalWowObjects));

    if(StartMode==0){
        updateSQLModelTable(Mode);
    }

//    qDebug()<<"TotalPlayer"<<TotalPlayer;
    PlayerInfo=0;

}

void MainWindow::keyPressEvent (QKeyEvent* ){

//    QString hexvalue = QString("0x%1").arg(ev->nativeVirtualKey(), 8, 16, QLatin1Char('0'));
//    ushort MyKey = hexvalue.toUtf8().toUShort(nullptr,16);
//    qDebug()<<hexvalue
//           <<MyKey

}

void PressKey(ushort scancode){
    INPUT key;
    key.type = INPUT_KEYBOARD;
    key.ki.wScan = 0; // hardware scan code for key
    key.ki.time = 0;
    key.ki.dwExtraInfo = 0;
    key.ki.wVk = scancode;
    key.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &key, sizeof(INPUT));
    key.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    SendInput(1, &key, sizeof(INPUT));
}

void PressMouse(int x, int y){
    INPUT input;
    input.type=INPUT_MOUSE;
    input.mi.dx=x+10000;
    input.mi.dy=y+10000;
    input.mi.dwFlags=(MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_MOVE|MOUSEEVENTF_RIGHTDOWN|MOUSEEVENTF_RIGHTUP);
    input.mi.mouseData=0;
    input.mi.dwExtraInfo=0;
    input.mi.time=0;
    SendInput(1,&input,sizeof(INPUT));
}

void SendCommandChat(QString command){

//    qDebug() << command;
    SetForegroundWindow(hCurWnd);

    //ENG
    PostMessage(hCurWnd, WM_INPUTLANGCHANGEREQUEST, 0, reinterpret_cast<LPARAM>(LoadKeyboardLayoutA("00000409", KLF_ACTIVATE)));
    delay(100);

    SendKey(0x0D); //enter
    delay(100);
//    PressKey(static_cast<ushort>(0x6f));
    for(int i = 0; i < command.length(); i++){
        SHORT vKey = VkKeyScanExW(static_cast<ushort>(command.toStdString().c_str()[i]), GetKeyboardLayout(0));
        if(command[i]=='(' || command[i]==')' || command[i].isUpper()){
            INPUT key;
            key.type = INPUT_KEYBOARD;
            key.ki.wScan = 0; // hardware scan code for key
            key.ki.time = 0;
            key.ki.dwExtraInfo = 0;
            key.ki.wVk = 0xa0;
            key.ki.dwFlags = 0; // 0 for key press
            SendInput(1, &key, sizeof(INPUT));
            PressKey(static_cast<ushort>(vKey));
            key.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
            SendInput(1, &key, sizeof(INPUT));
        }
        else
            PressKey(static_cast<ushort>(vKey));
        delay(30);
    }
    SendKey(0x0D); //enter
}

static QString TimerMode, ASCtoDESC="ASC",notIncludeLocation="",
        PreTpQuery;
static int LoadTableMs, ToResetCount=0, CollumnHidden = 0;
void MainWindow::updateSQLModelTable(QString Mode)
{
    QTime start = QTime::currentTime();

    QStringList Labels;
    QString Sort="";
    int ColumnCount=0;

    QStringList dopFiltr = setFiltrTableToSqlStr();
    QString filtr="AND NodeName IN(";
    if(dopFiltr.count()==0)
        filtr="AND NodeName IS NULL";
    else{
        for (int z=0;z<dopFiltr.count();z++) {
            if(z!=dopFiltr.count()-1)
                filtr=filtr+"'"+dopFiltr[z]+"', ";
            else
                filtr=filtr+"'"+dopFiltr[z]+"')";
        }
    }
    if(Mode=="Npc"||Mode=="Player"){
        ColumnCount=6;
        Labels<<"Локация"<<"Имя"<<"ID"<<"X"<<"Y"<<"Z";
//        ui->frame_2->hide();
        ui->actionSetFilter->setDisabled(true);
        ui->actionZeroNodes->setDisabled(true);
        Sort = " ORDER BY Name";
    }
    else{
        Mode="Node";
//        ui->frame_2->show();
        ui->actionSetFilter->setEnabled(true);
        ui->actionZeroNodes->setEnabled(true);

        QString deathIgnore = "";
        if(ui->checkBox->isChecked())
            deathIgnore = "AND XY < 5 ";

        Sort = "WHERE Continent = '"+LocalPlayer->Continent+"' %1 %2 AND LastUse!='4_ToReset' and LastUse!='5_ToReset' AND MapName!='%3' "
                                                            +deathIgnore+" ORDER BY LastUse , MapName %4"
                ;
        Sort=Sort.arg(TableViewMode).arg(filtr).arg(notIncludeLocation).arg(ASCtoDESC);
        ColumnCount=9;
        Labels<<"ID"<<"Континент"<<"Локация"<<"Тип"<<"Нода"<<"X"<<"Y"<<"Z"<<"LastUse"<<"Deaths";
    }

    ctr  = "SELECT * FROM %1Data %2";
    ctr = ctr.arg(Mode).arg(Sort);

    PreTpQuery = ctr;
//    qDebug()<<ctr;

    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }

    model->setQuery(*query);
    if (model->lastError().isValid()){
        qDebug() << model->lastError();
    }

    for (int i=0;i<ColumnCount;i++) {
        model->setHeaderData(i, Qt::Horizontal, tr(Labels[i].toStdString().c_str()));
    }

    ui->tableView->setModel(model);

    if(Mode!="Node"){
        CollumnHidden = 0;
        ui->tableView->setColumnHidden(1,false);
        ui->tableView->setColumnHidden(3,false);
        ui->tableView->setColumnHidden(5,false);
        ui->tableView->setColumnHidden(6,false);
        ui->tableView->setColumnHidden(7,false);
        ui->tableView->setColumnHidden(9,false);
    }
    else if(CollumnHidden !=1){
        CollumnHidden = 1;
//        ui->tableView->setColumnHidden(0,true);
        ui->tableView->setColumnHidden(1,true);
//        ui->tableView->setColumnHidden(2,true);
        ui->tableView->setColumnHidden(3,true);
        ui->tableView->setColumnHidden(5,true);
        ui->tableView->setColumnHidden(6,true);
        ui->tableView->setColumnHidden(7,true);
//        ui->tableView->setColumnHidden(8,true);
        ui->tableView->setColumnHidden(9,true);
    }

    ctr  = "SELECT count(ID) FROM %1Data %2";
    ctr = ctr.arg(Mode).arg(Sort);
//    qDebug()<<ctr;
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    query->next();
    if(query->value(0).toInt()>0){
        ui->tableView->show();
        ui->groupBox_6->setTitle("Точек в списке - "+QString::number(query->value(0).toInt()));
    }
    else{
        ui->tableView->hide();
        ui->groupBox_6->setTitle("");
    }

    ctr  = "SELECT count(ID) FROM %1Data %2";
//    ctr = ctr.arg(Mode).arg(Sort.replace("ORDER BY LastUse, MapName","AND LastUse!='4_ToReset' and LastUse!='5_ToReset'"));
    ctr = ctr.arg(Mode).arg(Sort);
//    qDebug()<<ctr;
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    query->next();
    ToResetCount=query->value(0).toInt();

    ui->tableView->selectRow(0);

    LoadTableMs = start.msecsTo(QTime::currentTime());
//    ui->textEdit->append("Load table - "+QString::number(LoadTableMs)+" msecs");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushCheckNPC_clicked()
{
    RadarTimer_Tick("Npc");
}

void MainWindow::on_pushCheckPlayers_clicked()
{
    RadarTimer_Tick("Player");
}

void MainWindow::on_pushCheckNodes_clicked()
{
    RadarTimer_Tick("Node");
}

void MainWindow::TimerOut()
{
    RadarTimer_Tick(TimerMode);
}

void MainWindow::TimerReloadAdresesOut()
{
    TimerReloadAdreses->stop();
    if(ui->tabWidget->currentIndex()==1){
        LocalPlayer->XPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_X);
        LocalPlayer->YPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_Y);
        LocalPlayer->ZPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_Z);
        ui->lineCurCoords->setText(QString("%1, %2, %3")
                                   .arg(static_cast<double>(LocalPlayer->XPos))
                                   .arg(static_cast<double>(LocalPlayer->YPos))
                                   .arg(static_cast<double>(LocalPlayer->ZPos)));
    }
    QString state = RMstring(0xB6A9E0);
//    qDebug()<<"state"<<state;
    if(state=="login" && StartMode==1){
        if(setPassAndLogin()==true){
//            delay(500);
//            SendKey(0x0D); //enter
//            delay(500);

            stopFarmF1();
            delay(1000);
            LoadAddresses();
            delay(1000);
            on_StartButton_clicked();

//            if(IsLoadingOrConnecting()==true){
//                while(IsLoadingOrConnecting()==true){
//                    delay(500);
//                    qDebug()<<"жду загрузки";
//                }
//            }
        }
    }
    TimerReloadAdreses->start(500);
}

//int FirstProfaLevel(){
//    int level = RMint(RM(RM(RM(0x400000+0x6CFDA0)+0x4)+0x4)+0x10);
//    return level;
//}

void MainWindow::on_pushTimerNodes_clicked()
{
    if(Timer->isActive()&&TimerMode=="Node"){
        Timer->stop();
        ui->pushTimerNodes->setStyleSheet("");
    }
    else if(TimerMode!="Node" || (!Timer->isActive()&&TimerMode=="Node")){
        TimerMode="Node";
        RadarTimer_Tick(TimerMode);
        Timer->stop();
        Timer->start(TimerMS);
        ui->pushTimerNodes->setStyleSheet("background-color: rgb(0, 170, 0);");
        ui->pushTimerNpc->setStyleSheet("");
        ui->pushTimerPlayers->setStyleSheet("");
    }
}

void MainWindow::on_pushTimerNpc_clicked()
{
    if(Timer->isActive()&&TimerMode=="Npc"){
        Timer->stop();
        ui->pushTimerNpc->setStyleSheet("");
    }
    else if(TimerMode!="Npc" || (!Timer->isActive()&&TimerMode=="Npc")){
        TimerMode="Npc";
        RadarTimer_Tick(TimerMode);
        Timer->start(TimerMS);
        ui->pushTimerNodes->setStyleSheet("");
        ui->pushTimerNpc->setStyleSheet("background-color: rgb(0, 170, 0);");
        ui->pushTimerPlayers->setStyleSheet("");
    }
}

void MainWindow::on_pushTimerPlayers_clicked()
{
    if(Timer->isActive()&&TimerMode=="Player"){
        Timer->stop();
        ui->pushTimerPlayers->setStyleSheet("");
    }
    else if(TimerMode!="Player" || (!Timer->isActive()&&TimerMode=="Player")){
        TimerMode="Player";
        RadarTimer_Tick(TimerMode);
        Timer->start(TimerMS);
        ui->pushTimerNodes->setStyleSheet("");
        ui->pushTimerNpc->setStyleSheet("");
        ui->pushTimerPlayers->setStyleSheet("background-color: rgb(0, 170, 0);");
    }
}

void MainWindow::on_ToMacros_clicked()
{
    if(ToMacros==0){
        ToMacros = 1;
        FlyOn();
        ui->ToMacros->setStyleSheet("background-color: rgb(0, 170, 0);");
    }
    else{
        ToMacros = 0;
        FlyOFF();
        ui->ToMacros->setStyleSheet("");
    }
}

void MainWindow::on_ZposToMacros_valueChanged(int arg1)
{
    ZposToMacros = arg1;
}

void MainWindow::on_WileStepBox_valueChanged(int arg1)
{
    TimerMS = arg1;
}

static QString buttonmode;
void MainWindow::on_HERBButton_clicked()
{
    if(ui->HERBButton->styleSheet()==""){
        ui->HERBButton->setStyleSheet("background-color: rgb(0, 170, 0);");
        if(ui->MINEButton->styleSheet()!="")
            TableViewMode="AND NodeType IN('HERB', 'MINE')";
        else
            TableViewMode="AND NodeType IN('HERB')";
    }
    else{
        if(ui->MINEButton->styleSheet()!="")
            TableViewMode="AND NodeType IN('MINE')";
        else{
            TableViewMode="IS NULL";
        }
        ui->HERBButton->setStyleSheet("");
    }
    updateFiltrTable();
    RadarTimer_Tick("Node");
}

void MainWindow::on_MINEButton_clicked()
{
    if(ui->MINEButton->styleSheet()==""){
        ui->MINEButton->setStyleSheet("background-color: rgb(0, 170, 0);");
        if(ui->HERBButton->styleSheet()!="")
            TableViewMode="AND NodeType IN('HERB', 'MINE')";
        else
            TableViewMode="AND NodeType IN('MINE')";
    }
    else{
        if(ui->HERBButton->styleSheet()!="")
            TableViewMode="AND NodeType IN('HERB')";
        else
            TableViewMode="IS NULL";
        ui->MINEButton->setStyleSheet("");
    }
    updateFiltrTable();
    RadarTimer_Tick("Node");
}

void MainWindow::on_StartButton_clicked()
{
    if(StartMode==0){
//        ui->textEdit->clear();
        if(!ui->tableView->model()){
            ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"В списке нет точек. Жмякните фильтр/скан.");
            return;
        }
        if(ui->tableView->model()->rowCount()==0){
            ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"В списке нет точек. Жмякните фильтр/скан.");
            return;
        }

        ui->StartButton->setStyleSheet("background-color: rgb(0, 170, 0);");
        ui->StartButton->setText("STOP");
        ui->StartButton->setIcon(QPixmap(":/png/32x32/pause.png"));
        ui->StartButton->setDisabled(true);
        delay(2000);
        ui->StartButton->setEnabled(true);

//        Timer->stop();
//        ui->pushTimerNodes->setStyleSheet("");
//        ui->pushTimerNpc->setStyleSheet("");
//        ui->pushTimerPlayers->setStyleSheet("");

        StartMode = 1;
        on_tableView_activated(model->index(0,0));
    }
    else{
        stopFarmF1();
    }
}

void MainWindow::stopFarmF1()
{
//    if(StartMode==1){
        ui->StartButton->setStyleSheet("");
        ui->StartButton->setText("START");
        ui->StartButton->setIcon(QPixmap(":/png/32x32/play.png"));
        StartMode = 0;
        FlyOFF();
//    }
}

void MainWindow::on_pushButtonXY_clicked()
{
//    PostMessage(hCurWnd, WM_RBUTTONDOWN, MK_RBUTTON, MAKELPARAM(ui->spinBoxX->value(), ui->spinBoxY->value()));
//    PostMessage(hCurWnd, WM_MOUSEMOVE, 0, MAKELPARAM(ui->spinBoxX->value(), ui->spinBoxY->value()));
//    PostMessage(hCurWnd, WM_RBUTTONUP, MK_RBUTTON, MAKELPARAM(ui->spinBoxX->value(), ui->spinBoxY->value()));
//    PostMessage(hCurWnd, WM_MOUSEMOVE, 0, MAKELPARAM(ui->spinBoxX->value(), ui->spinBoxY->value()));

//    qDebug()<<FirstProfaLevel();

//    SetCursorPos(ui->spinBoxX->value(), ui->spinBoxY->value());

//    PostMessage(hCurWnd, WM_RBUTTONUP, 0, MAKELPARAM(0, 0));

    //script SendChatMessage("Hello Bob!", "WHISPER", "Common", "Bob");

//    typedef void (*SendChatMessage_)(QString msg, QString chatType, QString language, QString channel);
//    SendChatMessage_ SendChatMessage;
//    SendChatMessage = (SendChatMessage_)GetProcAddress(hCurWnd ,"SendChatMessage");

//    SendChatMessage = reinterpret_cast<SendChatMessage_>(0x400000+0x0050D170);
//    f();

//    SendKey(0xbb);
//    delay(20);
//    SendKey(0xbb);
}

void MainWindow::on_actionSetFilter_triggered()
{
    if(ui->frame_2->isHidden()){
        ui->tableWidget_2->show();
        ui->frame_2->show();
    }
    else{
        ui->tableWidget_2->hide();
        ui->frame_2->hide();
    }
}

void MainWindow::on_tableWidget_2_cellClicked(int row, int)
{
    if(ui->tableWidget_2->item(row,2)->text()=="1"){
        ui->tableWidget_2->item(row,0)->setBackground(QColor(255,255,255));
        ui->tableWidget_2->item(row,1)->setBackground(QColor(255,255,255));
        ui->tableWidget_2->item(row,2)->setText("0");

        ctr = "update NodeFiltr set State=0 where Name='%1'";
        ctr=ctr.arg(ui->tableWidget_2->item(row,0)->text());
        if(!query->exec(ctr)){
            qDebug() << query->lastError();
            qDebug() << ctr;
        }
    }
    else{
        ui->tableWidget_2->item(row,0)->setBackground(QColor(0,255,0,100));
        ui->tableWidget_2->item(row,1)->setBackground(QColor(0,255,0,100));
        ui->tableWidget_2->item(row,2)->setText("1");
        ctr = "update NodeFiltr set State=1 where Name='%1'";
        ctr=ctr.arg(ui->tableWidget_2->item(row,0)->text());
        if(!query->exec(ctr)){
            qDebug() << query->lastError();
            qDebug() << ctr;
        }
    }
    ui->tableWidget_2->clearSelection();
    updateSQLModelTable("Node");
}


void MainWindow::on_selectAllFiltr_clicked()
{
    for(int b=0;b<ui->tableWidget_2->rowCount();b++){
        if(ui->tableWidget_2->item(b,2)->text()=="0"){
            ui->tableWidget_2->item(b,0)->setBackground(QColor(0,255,0,100));
            ui->tableWidget_2->item(b,1)->setBackground(QColor(0,255,0,100));
            ui->tableWidget_2->item(b,2)->setText("1");
            ctr = "update NodeFiltr set State=1 where Name='%1'";
            ctr=ctr.arg(ui->tableWidget_2->item(b,0)->text());
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
        }
    }
    updateSQLModelTable("Node");
}

void MainWindow::on_unselectAllFiltr_clicked()
{
    for(int v=0;v<ui->tableWidget_2->rowCount();v++){
        if(ui->tableWidget_2->item(v,2)->text()=="1"){
            ui->tableWidget_2->item(v,0)->setBackground(QColor(255,255,255));
            ui->tableWidget_2->item(v,1)->setBackground(QColor(255,255,255));
            ui->tableWidget_2->item(v,2)->setText("0");

            ctr = "update NodeFiltr set State=0 where Name='%1'";
            ctr=ctr.arg(ui->tableWidget_2->item(v,0)->text());
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
        }
    }
    updateSQLModelTable("Node");
}

void MainWindow::on_tableWidget_2_cellActivated(int row, int )
{
    if(ui->tableWidget_2->item(row,2)->text()=="1"){
        ui->tableWidget_2->item(row,0)->setBackground(QColor(255,255,255));
        ui->tableWidget_2->item(row,1)->setBackground(QColor(255,255,255));
        ui->tableWidget_2->item(row,2)->setText("0");

        ctr = "update NodeFiltr set State=0 where Name='%1'";
        ctr=ctr.arg(ui->tableWidget_2->item(row,0)->text());
        if(!query->exec(ctr)){
            qDebug() << query->lastError();
            qDebug() << ctr;
        }
    }
    else{
        ui->tableWidget_2->item(row,0)->setBackground(QColor(0,255,0,100));
        ui->tableWidget_2->item(row,1)->setBackground(QColor(0,255,0,100));
        ui->tableWidget_2->item(row,2)->setText("1");
        ctr = "update NodeFiltr set State=1 where Name='%1'";
        ctr=ctr.arg(ui->tableWidget_2->item(row,0)->text());
        if(!query->exec(ctr)){
            qDebug() << query->lastError();
            qDebug() << ctr;
        }
    }
    ui->tableWidget_2->clearSelection();
    updateSQLModelTable("Node");
}

QStringList MainWindow::setFiltrTableToSqlStr(){
    QStringList string;
    for(int x=0;x<ui->tableWidget_2->rowCount();x++){
        if(ui->tableWidget_2->item(x,2)->text()=="1")
            string<<ui->tableWidget_2->item(x,0)->text();
    }
    return string;
}

void MainWindow::on_actionZeroNodes_triggered()
{
    if(!query->exec("update NodeData set lastuse = '3_DeaultPrior'")){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    updateSQLModelTable("Node");
}

void MainWindow::on_actioncheckDuplicatesNodeData_triggered()
{
    ctr  = "SELECT ID, X, Y, Z FROM NodeData";
    if(!query->exec(ctr)){
        qDebug() << query->lastError();
        qDebug() << ctr;
    }
    int i=0;
    while(query->next()){
        int ID=query->value(0).toInt();
        double X=query->value(1).toDouble();
        double Y=query->value(1).toDouble();
        double Z=query->value(1).toDouble();

        ctr  = "SELECT count(ID) FROM NodeData where X='%1' and Y='%2' and Z='%3'";
        if(!secondQuery->exec(ctr)){
            qDebug() << secondQuery->lastError();
            qDebug() << ctr;
        }
        secondQuery->next();
        int copyCount=secondQuery->value(0).toInt();
        if(copyCount!=0){
            qDebug()<<"ID"<<ID<<"X"<<X<<"Y"<<Y<<"Z"<<Z<<"имеет:"<<copyCount<<"дубликатов.";
        }
        else{
            qDebug()<<i+1<<"ID"<<ID<<"нет дубликатов";
        }

        i++;
    }
}

void MainWindow::on_actionqDebug_RadarTimer_Tick_STEP_triggered()
{
    if(!ui->actionqDebug_RadarTimer_Tick_STEP->isChecked()){
        ui->actionqDebug_RadarTimer_Tick_STEP->setChecked(true);
    }
    else {
        ui->actionqDebug_RadarTimer_Tick_STEP->setChecked(false);
    }
}

void MainWindow::on_actionqDebug_CurrentNode_triggered()
{
    if(!ui->actionqDebug_CurrentNode->isChecked()){
        ui->actionqDebug_CurrentNode->setChecked(true);
    }
    else {
        ui->actionqDebug_CurrentNode->setChecked(false);
    }
}

void MainWindow::on_actionStart_CheckAndCorrect_NodeNames_triggered()
{

}

bool MainWindow::IsLoadingOrConnecting(){
    if(RMint(0x00B6AA38)!=0){
        IsLoading=true;
        LoadAddresses();
        return true;
    }
    else{
        IsLoading=false;
        return false;
    }
}

bool MainWindow::setPassAndLogin()
{
replay:
    delay(2000);
    SetForegroundWindow(hCurWnd);

    //ENG
    PostMessage(hCurWnd, WM_INPUTLANGCHANGEREQUEST, 0, reinterpret_cast<LPARAM>(LoadKeyboardLayoutA("00000409", KLF_ACTIVATE)));

    //выбор мира
    if(RMint(RM(0x400000+0x007499A8)+0xEB8) == 1){
        qDebug()<<"выбор мира";
        SendKey(0x1B); //esc
        delay(500);
    }

    DWORD cursorAdress=RM(RM(RM(RM(0x400000+0x009CE474)+0x1d4)+0x78)+0x68);

    WriteProcessMemory(hProc, reinterpret_cast<void*>(cursorAdress), UserName.toStdString().c_str(), 30, nullptr);
    qDebug()<<reinterpret_cast<void*>(cursorAdress)<<UserName;

    SendKey(0x09); //tab
    delay(2000);

    cursorAdress=RM(RM(RM(RM(0x400000+0x009CE474)+0x1d4)+0x78)+0x68);
    WriteProcessMemory(hProc, reinterpret_cast<void*>(cursorAdress), Password.toStdString().c_str(), 30, nullptr);
    qDebug()<<reinterpret_cast<void*>(cursorAdress)<<Password;

    SendKey(0x0D); //enter
    delay(2000);

    int delayWait = 0;
    while(RMstring(0xB6A9E0)!="charselect"){
        qDebug()<<"жду загрузки для выбора персонажа";
        delay(1000);

        //выбор мира
        if(RMint(RM(0x400000+0x007499A8)+0xEB8) == 1){
            qDebug()<<"выбор мира";
            SendKey(0x1B); //esc
            delay(500);
            goto replay;
        }
        if(delayWait > 10000)
            goto replay;
        else
            delayWait = delayWait + 1000;

    }

    delay(1000);
    SendKey(0x0D); //enter
    delay(2000);

    delayWait = 0;
    while(IsLoadingOrConnecting()==true){
        qDebug()<<"жду загрузки в мир";
        delay(1000);

        //выбор мира
        if(RMint(RM(0x400000+0x007499A8)+0xEB8) == 1){
            qDebug()<<"выбор мира";
            delay(500);
            SendKey(0x1B); //esc
            goto replay;
        }
        if(delayWait > 10000)
            goto replay;
        else
            delayWait = delayWait + 1000;
    }
    delay(1000);

    int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
    if(hp < 40000 || RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth < 40000 && RMstring(0xB6A9E0)=="charselect"))
        return true;
    else
        return false;
}

static int allNodes = 0, runbitch=0, ViewTime=0;

void MainWindow::on_tableView_activated(const QModelIndex &index)
{
    QTime start = QTime::currentTime();

    int row = index.row();

    QString cur_id = ui->tableView->model()->data(ui->tableView->model()->index(row,0)).toString();
    QString cur_location = ui->tableView->model()->data(ui->tableView->model()->index(0,2)).toString();
    QString cur_mapname = ui->tableView->model()->data(ui->tableView->model()->index(0,4)).toString();
    QString cur_rowPrior = ui->tableView->model()->data(ui->tableView->model()->index(row,8)).toString();

    int dop_pause_kostil = 0;
    if(cur_rowPrior != "3_DeaultPrior")
        dop_pause_kostil = 900;

    float X=ui->tableView->model()->data(ui->tableView->model()->index(row,5)).toFloat();
    float Y=ui->tableView->model()->data(ui->tableView->model()->index(row,6)).toFloat();
    float Z=ui->tableView->model()->data(ui->tableView->model()->index(row,7)).toFloat();

    if(ui->actionqDebug_CurrentNode->isChecked())
        qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Node Current "<<cur_mapname<<cur_id;

    if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login0"; return;}

    LoadAddresses();

    SendKey(0x33);
    SendKey(0x34);
    SendKey(0x35);

    Jumps++;
    allNodes++;

retry2:
    if(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health)<(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth)/2) && StartMode==1){
        int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
        int maxhp = RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth);
        if(hp==1||hp==0){
            FlyOn();
            ui->textEdit->append("Сдох");

            SendKey(0x53); //S key
            delay(1000);
            SendCommandChat("/script RepopMe()");
            delay(100);
            if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login1"; return;}

            hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
            if(hp<2)
                TeleportToDeathPoint();
            else
                goto retry2;
            delay(3000);
            SendKey(0x53); //S key
            delay(100);
            if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login2"; return;}
            SendKey(0x53); //S key
            delay(3000);
            SendCommandChat("/script RetrieveCorpse()");
            delay(100);
            FlyOFF();
            if(StartMode==1) {
                goto retry2;
            }
            else{
                return;
            }
        }
        else if(hp>2 && hp<40000 && maxhp<40000){
            Z=Z+ZOffsetTP;
            TeleportTo(X,Y,Z);
            ui->textEdit->append("Мало ХП (<50%). Реген жмяк 2.");
            FlyOn();
            if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login3"; return;}
            SendKey(0x53);
            delay(1000);
            if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login4"; return;}
            SendKey(0x32);
            for (int i=0;i<20;i++){
                if(StartMode!=1)
                    return;
                if(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health)!=RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth)){
                    delay(500);
                    ui->textEdit->append(QString::number(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health))+"/"+QString::number(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth)));
                }
                else
                    break;
            }
            if(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth)!=RMuint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health))
                goto retry2;
            else {
                StartMode=0;
                on_StartButton_clicked();
            }
        }
        else if(hp>40000 || maxhp>40000){
            qDebug()<<"ДИсконнект???????????   hp>40000 || RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth)>40000";
//            return;
//            stopFarmF1();
            TimerReloadAdresesOut();

//            if(RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::MaxHealth)!=RMuint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health))
//                goto retry2;
//            else {
//                StartMode=0;
//                on_StartButton_clicked();
//            }
        }
    }

    ZOffsetTP = 900;
    FlyOn();
    Timer->stop();

    if(StartMode==1)
        Z=Z+ZOffsetTP;
    else if(ui->tableView->model()->columnCount()!=10){
        X=ui->tableView->model()->data(ui->tableView->model()->index(row,3)).toFloat();
        Y=ui->tableView->model()->data(ui->tableView->model()->index(row,4)).toFloat();
        Z=ui->tableView->model()->data(ui->tableView->model()->index(row,5)).toFloat();
    }

//    if(LocalPlayer->XPos-X<0.1f && LocalPlayer->YPos-Y<0.1f && LocalPlayer->ZPos-Z<0.1f){
        TeleportTo(X,Y,Z);
        SendKey(0x53); //S key
        SendKey(0x53); //S key
        SendKey(0x53); //S key
//    }


    runbitch=0;
    int viewMax=0;

    if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login5"; return;}

//    ui->textEdit->append("1,8");

    if(StartMode==1){

        ui->textEdit->append(QTime::currentTime().toString("\n[hh:mm:ss] ")+"ТП над точкой: ID"+cur_id+" "+cur_mapname);

        //ожидание ms для появления ноды
        delay(100+dop_pause_kostil);

        int waitNodeMs = WaitMsLoadNode;
        waitNodeMs = waitNodeMs - LoadTableMs - 100;
        int itterrDelay, itterr = 8;
        itterrDelay = waitNodeMs / itterr;

//        ui->textEdit->append("2");

        for(int q=0;q<itterr;q++){

//            ui->textEdit->append("3"+QString::number(q));
            if(StartMode==0)
                return;

            if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login6"; return;}

//            ui->textEdit->append("4"+QString::number(q));

            RadarTimer_Tick("Player+Node");
            delay(itterrDelay);

//            ui->textEdit->append("5"+QString::number(q));

            if(TotalPlayer==0 && NoNode==0){
//                ui->textEdit->append("6"+QString::number(q));
                viewMax=start.msecsTo(QTime::currentTime());
//                qDebug()<<"Время для появления ноды в менеджере - "+QString::number(start.msecsTo(QTime::currentTime()))+" ms";
//                ui->textEdit->append("Node view - "+QString::number(start.msecsTo(QTime::currentTime()))+" msecs");
                break;
            }
            else if(TotalPlayer>0 && NoNode==0){
//                ui->textEdit->append("7"+QString::number(q));
                break;
            }
        }
//        ui->textEdit->append("8");
        if(StartMode==0)
            return;
        if(TotalPlayer==0 && NoNode==0){
//            ui->textEdit->append("9");
            ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Игроков в радиусе нет. ТП к точке: ID"+cur_id+" "+cur_mapname);
            Z=Z-ZOffsetTP;
            WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharZ), &Z, sizeof(Z), nullptr);
            delay(10);
            if(RMstring(0xB6A9E0)=="login"){ qDebug()<<"login7"; return;}

            SendKey(0x53); //S key
            for(int q=0;q<8;q++){
                SendKey(0xbb); //ПКМ забиндена на +=
                delay(20);
                SendKey(0xbb); //ПКМ забиндена на +=
                delay(20);
            }
            for(int q=0;q<8;q++){
                SendKey(0xbb); //ПКМ забиндена на +=
                delay(20);
                SendKey(0xbb); //ПКМ забиндена на +=
                delay(20);
                if(RM(LocalPlayer->BaseAddress+0xA6C)!=0){
                    break;
                }
            }
            if(StartMode==0)
                return;
            int nodeLooted = 0;
            SendKey(0xbb); //ПКМ забиндена на +=
            delay(20);
            while(RM(LocalPlayer->BaseAddress+0xA6C)!=0){
                if(StartMode==0)
                    return;
                if(nodeLooted==0)
                    ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Кастую сбор!");
                delay(1);
                nodeLooted=1;
                RadarTimer_Tick("Player+Node");
                delay(200);
                if(TotalPlayer>0){
                   qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Кто-то подлетел пока ты кастовал. Сматываюсь на сл точку!";
                    ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Кто-то подлетел пока ты кастовал. Сматываюсь на сл точку!");
                    PlayerInfo=1;
                    NoLootedIzzaPlayers++;
                    RadarTimer_Tick("Player+Node");
                    delay(300);
                    Z=Z+ZOffsetTP;
                    WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharZ), &Z, sizeof(Z), nullptr);
                    SendKey(0x53); //S key
                    delay(550);
                    if(StartMode==0)
                        return;
                    runbitch=1;
                    break;
                }
            }
            delay(200);
//            qDebug()<<RMstring(0xBCFB90);
            if(RMstring(0xBCFB90)=="Прервано"){
                ui->textEdit->append("--- СБОР ПРЕРВАН ---");
//                qDebug()<<"Сбор прерван";
                nodeLooted=2;
            }
            if(nodeLooted==1 && runbitch==0){
                if(cur_rowPrior.remove(1,25)!="2"){
                    Looted++;

//                    if(!query->exec(QString("update NodeData set loottime = '%1' WHERE ID = %2").arg(QTime::currentTime().toString("hh:mm:ss")).arg(cur_id))){
//                        qDebug() << query->lastError();
//                        qDebug() << ctr;
//                    }

                    NodeStopRound2<<cur_id.toInt();
                }

//                ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Докастовал!");
                Z=Z+ZOffsetTP;
//                delay(300);
                if(StartMode==0)
                    return;
                if(RMstring(0xB6A9E0)=="login") return;
                WriteProcessMemory(hProc, reinterpret_cast<void*>(adresCharZ), &Z, sizeof(Z), nullptr);
                SendKey(0x53); //S key
            }
        }
        else{
            if(NoNode==1){
                ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Тут ничего нет!");
            }
            else{
                ui->textEdit->append(QTime::currentTime().toString("[hh:mm:ss] ")+"Игроки в радиусе - "+QString::number(TotalPlayer)+". Прыг на следующую точку");
                qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"Игроки в радиусе - "+QString::number(TotalPlayer)+". Прыг на следующую точку";
                NoLootedIzzaPlayers++;
                PlayerInfo=2;
                RadarTimer_Tick("Player+Node");

//                Z=Z+ZOffsetTP;
//                TeleportTo(X,Y,Z);
                runbitch=1;
                if(StartMode==0)
                    return;
            }
        }

        if(cur_rowPrior=="2_Round2" || runbitch==1){
            ctr = "UPDATE NodeData SET LastUse = '5_ToReset' WHERE ID = '%1'";
            ctr=ctr.arg(cur_id);
    //        qDebug() << ctr;
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }

        }
        else{
            ctr = "UPDATE NodeData SET LastUse = '4_ToReset' WHERE ID = '%1'";
            ctr=ctr.arg(cur_id);
    //        qDebug() << ctr;
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
        }

//        if(!ui->checkBox->isChecked()){
            int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
            if(hp==1||hp==0){
                ui->textEdit->append("Сдох на точке ID "+cur_id);

                //пометить точку в БД как предохлячную
                ctr = "SELECT XY FROM NodeData WHERE ID = '%1'";
                ctr=ctr.arg(cur_id);
                qDebug() << ctr;
                if(!query->exec(ctr)){
                    qDebug() << query->lastError();
                    qDebug() << ctr;
                }
                int cur_deaths=0;
                while(query->next()){
                    cur_deaths = query->value(0).toInt();
                }
                cur_deaths = cur_deaths + 1;

                ctr = "UPDATE NodeData SET XY = '%2' WHERE ID = '%1'";
                ctr=ctr.arg(cur_id).arg(cur_deaths);
                qDebug() << ctr;
                if(!query->exec(ctr)){
                    qDebug() << query->lastError();
                    qDebug() << ctr;
                }
            }
//        }

        if(StartMode==1){

            NoNode=1;

//            if(GetBuffs(LocalPlayer->BaseAddress).count()==0){
//                qDebug()<<"Ошибка в GetBuffs. Не видит аур.";
//            }


//            if(!GetBuffs(LocalPlayer->BaseAddress).contains(1784)){ //инвиз роги
            if(!GetBuffs(LocalPlayer->BaseAddress).contains(5215)){ //инвиз кота
                SendKey(0x31);
            }

            if(ViewTime<viewMax)
                ViewTime = viewMax;
            if(Looted>0 || allNodes>0)
                ui->labelLooted->show();

            ui->labelLooted->setText(
                        "Полутал нод: "+QString::number(Looted)
                        +"\nПрыжков: "+QString::number(Jumps)
                        +"\nКПД: "+QString::number(Looted/static_cast<double>(Jumps)*100,'f',1)+" %"

                        +"\n\nТочек проверено: "+QString::number(allNodes)
                        +"\nНовых точек: "+QString::number(NewNodesCounter)
                        +"\nПропущено из-за уродов: "+QString::number(NoLootedIzzaPlayers)

                        +"\n\nNodeLoad: "+QString::number(ViewTime)+"ms"
                        );

            //сброс приоритета при полном проходе
            if(ToResetCount<=2)
//            if(allNodes % ui->tableView->model()->rowCount() == 0)
//                on_actionZeroNodes_triggered();
                resetTable();
//            else
//                stopFarmF1();


            float preX = 0.0,preY = 0.0,preZ = 0.0;
            PreTpQuery.replace("*","X,Y,Z");
            ctr = PreTpQuery + " LIMIT 1";
//            qDebug() << ctr;
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
            while(query->next()){
                preX = query->value(0).toFloat();
                preY = query->value(1).toFloat();
                preZ = query->value(2).toFloat()+ZOffsetTP;
            }
            double x1 = static_cast<double>(LocalPlayer->XPos);
            double y1 = static_cast<double>(LocalPlayer->YPos);


//            if(!query->exec(QString("SELECT ID, LootTime, NodeName FROM NodeData WHERE LootTime IS NOT NULL"))){
//                qDebug() << query->lastError();
//            }
//            while(query->next()){
//                int IDn = query->value(0).toInt();
//                QTime loottime = QTime::fromString(query->value(1).toString(), "hh:mm:ss");
//                int secsToReset = loottime.secsTo(QTime::currentTime());
//                QString name = query->value(2).toString();

//                qDebug() << IDn << name <<  "секунд после лута -" << secsToReset;

//                if(secsToReset > 600){
//                    ctr = "UPDATE NodeData SET LastUse = '2_Round2', loottime = NULL WHERE ID = '%1'";
//                    ctr=ctr.arg(IDn);
//                    //   qDebug() << ctr;
//                    if(!query->exec(ctr)){
//                        qDebug() << query->lastError();
//                        qDebug() << ctr;
//                    }
//                    qDebug() << IDn << name << "10m прошло. Сброс приора на 2_Round2.";
//                }
//            }


            if(ToResetCount>1){
                //убираю из таблицы то, что не обнаружилось в дистанции обзора
                for(int rows = 1; rows < model->rowCount(); rows++ )
                {
                    QString prior = ui->tableView->model()->data(ui->tableView->model()->index(rows,8)).toString();
                    QString point_location = ui->tableView->model()->data(ui->tableView->model()->index(rows,2)).toString();
                    int IDn = ui->tableView->model()->data(ui->tableView->model()->index(rows,0)).toInt();
                    QString name = ui->tableView->model()->data(ui->tableView->model()->index(rows,4)).toString();



                    if(cur_location==point_location && prior=="3_DeaultPrior"){
                        double x2 = ui->tableView->model()->data(ui->tableView->model()->index(rows,5)).toDouble();
                        double y2 = ui->tableView->model()->data(ui->tableView->model()->index(rows,6)).toDouble();
                        double a = qAbs(x1) - qAbs(x2);
                        double b = qAbs(y1) - qAbs(y2);
                        double c = qPow(a,2) + qPow(b,2);
                        c = qSqrt(c);

                        if(c<=101){ //дистанция
                            ctr = "UPDATE NodeData SET LastUse = '5_ToReset' WHERE ID = '%1' AND LastUse = '3_DeaultPrior'";
                            ctr=ctr.arg(IDn);
                            //                    qDebug() << ctr;
                            if(!query->exec(ctr)){
                                qDebug() << query->lastError();
                                qDebug() << ctr;
                            }
                            //                        qDebug() << "Удалена из списка"<< IDn<< name<< cur_location<< prior<< c;
                            allNodes++;
                        }
                    }
                }
            }

            TeleportTo(preX,preY,preZ);
            SendKey(0x53); //S key
            SendKey(0x53); //S key
            SendKey(0x53); //S key

            updateSQLModelTable("Node");


//            ui->textEdit->append("TP interval - "+QString::number(start.msecsTo(QTime::currentTime()))+" msecs");
                on_tableView_activated(model->index(row,0));
        }
        else
            return;
    }

    FlyOFF();
}

void MainWindow::resetTable(){
    ui->textEdit->clear();
    if(!ui->checkReversList->isChecked()){
        ASCtoDESC="ASC";
        notIncludeLocation="";
    }
    else{
        QString last_location = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),2)).toString();
        notIncludeLocation=last_location;

        if(ASCtoDESC=="ASC"){
            ASCtoDESC="DESC";
//            notIncludeLocation="9_Седые холмы";
        }
        else{
            ASCtoDESC="ASC";
//            notIncludeLocation="1_Лес Хрустальной Песни";
        }
    }
    qDebug() << "ToResetCount==0"<<"on_actionZeroNodes_triggered()";
    on_actionZeroNodes_triggered();
}

void MainWindow::on_pushTP_Dalaran_clicked()
{
    LoadAddresses();
    FlyOn();
    int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
    if(hp==1||hp==0){
        ui->textEdit->append("Сдох");
        SendCommandChat("/script RepopMe()");
        delay(3000);
        TeleportToDeathPoint();
        SendKey(0x53); //S key
        delay(3000);
        SendCommandChat("/script RetrieveCorpse()");
        delay(500);
    }
    hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
    if(hp>1){
        resetTable();
        ASCtoDESC="ASC";
        notIncludeLocation="";
        QString Continent=RMstring(0xCE06D0);
        if(Continent=="Northrend")
            TeleportToD(637.4, 5865.0595, 647.47857);
        else if(Continent=="Azeroth")
            TeleportToD(-2075.781, -1511.055, 21.976568);
//        else if(Continent=="Expansion01")
//        TeleportToD(000, 000, 000);
        SendKey(0x53); //S key
        FlyOFF();
    }
}

void MainWindow::on_pushTPStormwind_clicked()
{
    LoadAddresses();
    FlyOn();
    int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
    if(hp==1||hp==0){
        ui->textEdit->append("Сдох");
        SendCommandChat("/script RepopMe()");
        delay(3000);
        TeleportToDeathPoint();
        SendKey(0x53); //S key
        delay(3000);
        SendCommandChat("/script RetrieveCorpse()");
        delay(500);
    }
    hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
    if(hp>1){
        resetTable();
        ASCtoDESC="ASC";
        notIncludeLocation="";
        QString Continent=RMstring(0xCE06D0);
//        qDebug()<<Continent;
        LocalPlayer->Faction = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Faction);
        if(LocalPlayer->Faction==1||LocalPlayer->Faction==3||LocalPlayer->Faction==4||LocalPlayer->Faction==115||LocalPlayer->Faction==1629){
            if(Continent=="Northrend")
                TeleportToD(721.59503, 5720.2250, 641.62182);
            else if(Continent=="Azeroth")
                TeleportToD(650.58807, -8813.235, 94.553665 );
            else if(Continent=="Expansion01")
                TeleportToD(5406.5390, -1792.971, -12.42714);
        }
        else{
            if(Continent=="Northrend")
                TeleportToD(592.581, 5925.65, 640.593);
//            else if(Continent=="Azeroth")
            else if(Continent=="Kalimdor")
                TeleportToD(-4459.46, 1667.48, 19.0577);
            else if(Continent=="Expansion01")
                TeleportToD(5453.24, -1933.86, -12.428);
        }
        SendKey(0x53); //S key
        FlyOFF();
    }
}

void MainWindow::on_pushTPGorn_clicked()
{
    LoadAddresses();
     FlyOn();
     int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
     if(hp==1||hp==0){
         ui->textEdit->append("Сдох");
         SendCommandChat("/script RepopMe()");
         delay(3000);
         TeleportToDeathPoint();
         SendKey(0x53); //S key
         delay(3000);
         SendCommandChat("/script RetrieveCorpse()");
         delay(500);
     }
     hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
     if(hp>1){
         resetTable();
         ASCtoDESC="ASC";
         notIncludeLocation="";
         QString Continent=RMstring(0xCE06D0);
         LocalPlayer->Faction = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Faction);
         if(LocalPlayer->Faction==1||LocalPlayer->Faction==3||LocalPlayer->Faction==4||LocalPlayer->Faction==115||LocalPlayer->Faction==1629){
             if(Continent=="Northrend")
                 TeleportToD(693.005, 5916.02, 642.428);
             else if(Continent=="Azeroth")
                 TeleportToD(606.476, -8427.37, 95.0711);
             else if(Continent=="Expansion01")
                 TeleportToD(5222.71, -1840.87, -38.0451);
         }
         else{
             if(Continent=="Northrend")
                 TeleportToD(693.005, 5916.02, 642.428);
//             else if(Continent=="Azeroth")
//                 TeleportToD(606.476, -8427.37, 95.0711);
             else if(Continent=="Expansion01")
                 TeleportToD(5222.71, -1840.87, -38.0451);
             else if(Continent=="Kalimdor")
                 TeleportToD(-4689.04, 2017.84, 25.3721);
         }
         SendKey(0x53); //S key
         FlyOFF();
     }
}

void MainWindow::on_pushTPGB_clicked()
{
    LoadAddresses();
     FlyOn();
     int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
     if(hp==1||hp==0){
         ui->textEdit->append("Сдох");
         SendCommandChat("/script RepopMe()");
         delay(3000);
         TeleportToDeathPoint();
         SendKey(0x53); //S key
         delay(3000);
         SendCommandChat("/script RetrieveCorpse()");
         delay(500);
     }
     hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
     if(hp>1){
         resetTable();
         ASCtoDESC="ASC";
         notIncludeLocation="";
         QString Continent=RMstring(0xCE06D0);
         LocalPlayer->Faction = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Faction);
         if(LocalPlayer->Faction==1||LocalPlayer->Faction==3||LocalPlayer->Faction==4||LocalPlayer->Faction==115||LocalPlayer->Faction==1629){
             if(Continent=="Northrend")
                 TeleportToD(596.036, 5959.77, 650.628);
             else if(Continent=="Azeroth")
                 TeleportToD(634.21, -8911.52, 99.9449);
             else if(Continent=="Expansion01")
                 TeleportToD(5322.42, -1987.87, -7.09943);
         }
         else{
             if(Continent=="Northrend")
                 TeleportToD(596.036, 5959.77, 650.628);
//             else if(Continent=="Azeroth")
//                 TeleportToD(634.21, -8911.52, 99.9449);
             else if(Continent=="Expansion01")
                 TeleportToD(5322.42, -1987.87, -7.09943);
             else if(Continent=="Kalimdor")
                 TeleportToD(-4381.12, 1635.06, 12.6516);
         }
         SendKey(0x53); //S key
         FlyOFF();
     }
}

void MainWindow::on_pushTPZapredelie_clicked()
{
    LoadAddresses();
     FlyOn();
     int hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
     if(hp==1||hp==0){
         ui->textEdit->append("Сдох");
         SendCommandChat("/script RepopMe()");
         delay(3000);
         TeleportToDeathPoint();
         SendKey(0x53); //S key
         delay(3000);
         SendCommandChat("/script RetrieveCorpse()");
         delay(500);
     }
     hp=RMint(LocalPlayer->UnitFieldsAddress + UnitOffsets::Health);
     if(hp>1){
         resetTable();
         ASCtoDESC="ASC";
         notIncludeLocation="";
         QString Continent=RMstring(0xCE06D0);
         LocalPlayer->Faction = RM(LocalPlayer->UnitFieldsAddress + UnitOffsets::Faction);
         if(LocalPlayer->Faction==1||LocalPlayer->Faction==3||LocalPlayer->Faction==4||LocalPlayer->Faction==115||LocalPlayer->Faction==1629){
             if(Continent=="Northrend")
                 TeleportToD(744.436, 5698.26, 641.836);
             else if(Continent=="Azeroth")
                 TeleportToD(-3207.57, -11905.2, -14.8622);
             else if(Continent=="Expansion01")
                 TeleportToD(5297.68, -2100.12, -37.3236);
         }
         else{
             if(Continent=="Kalimdor")
                 TeleportToD(-4216.11, 1471.91, 59.221);
             if(Continent=="Northrend")
                 TeleportToD(584.23, 5940.67, 640.548);
             else if(Continent=="Azeroth")
                 TeleportToD(-3207.57, -11905.2, -14.8622);
             else if(Continent=="Expansion01")
                 TeleportToD(5297.68, -2100.12, -37.3236);
         }
         SendKey(0x53); //S key
         FlyOFF();
     }
}

void MainWindow::on_setPassAndLogin_clicked()
{
    if(setPassAndLogin()==true){
        qDebug()<<"login is ok";
        LoadAddresses();
    }
    else
        qDebug()<<"error login";
}

void MainWindow::closeEvent(QCloseEvent *)
{
    stopFarmF1();
    exit(1);
}

#include <QClipboard>
void MainWindow::on_lineCurCoords_returnPressed()
{
    QString coords = ui->lineCurCoords->text();
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(coords);
}


static DWORD FindPlayersLineAdress=0, FirstCicle=0, StartScanBots=0;
static QStringList
                BotName,
                BotLocation;
void MainWindow::on_pushScanBots_clicked()
{
    if(StartScanBots==1){
        ui->pushScanBots->setStyleSheet("");
        TimerFindBots.stop();
        StartScanBots=0;
    }
    else{
        FindPlayersLineAdress = RM(RM(RM(RM(0x400000+0x009CE474)+0x1d4)+0x78)+0x68);
        qDebug()<<"cursorAdress"<<reinterpret_cast<void*>(FindPlayersLineAdress);
        if(FindPlayersLineAdress!=0){
            StartScanBots=1;
            ctr  = "DROP TABLE IF EXISTS HookBots";
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
            ctr  = "CREATE TABLE IF NOT EXISTS HookBots ("
                        "`Name`      TEXT,"
                        "`LocationPrev`  TEXT,"
                        "`LocationNext`  TEXT,"
                        "`Time`      TEXT)";
            if(!query->exec(ctr)){
                qDebug() << query->lastError();
                qDebug() << ctr;
            }
            ui->pushScanBots->setStyleSheet("background-color: rgb(0, 255, 0);");
            delay(500);
            SendKey(0x0D); //enter
            delay(500);
            BotLocation.clear();
            BotName.clear();
            FirstCicle=1;
            TimerFindBots.start(200);
        }
        else{
            qDebug()<<"Чета не подхватило курсорчик!";
        }
    }
}

void MainWindow::TimerFindBotsOut(){
    TimerFindBots.stop();
    QString location;
    QStringList
//            BotName,
            Locations;
    Locations <<"Борейская тундра"
             <<"Шолазар"
            <<"Озеро Ледяных Оков"
           <<"Ледяная Корона"
          <<"Драконий Погост"
         <<"Хрустальной"
        <<"Грозовая Гряда"
       <<"Зул'Драк"
      <<"Седые холмы"
     <<"Ревущий фьорд"
               ;
    for(int loc=0;loc<Locations.count();loc++){
        if(StartScanBots==0) return;
        location = Locations[loc];
//        qDebug()<<"cur LOC"<<location<<loc;
        WriteProcessMemory(hProc, reinterpret_cast<void*>(FindPlayersLineAdress), location.toStdString().c_str(), 30, nullptr);

        SendKey(0x33);
//        SendCommandChat("/click WhoFrameWhoButton");
        delay(350);
        if(StartScanBots==0) return;

        if(location=="Шолазар")
            location="Низина Шолазар";
        else if(location=="Хрустальной")
            location="Лес Хрустальной Песни";
        else if(location=="Зул'Драк")
            location="ЗулДрак";

        int BotsInLocation = RMint(0x00C7BFE4);
//        qDebug()<<"BotsInLocation"<<BotsInLocation;
        if(BotsInLocation>50)
            BotsInLocation=50;

        DWORD FirstBotAdress = 0x00C79FD8;
        QString FirstBot = RMstring(0x00C79FD8);

        for (int i=0;i<BotsInLocation;i++) {
            if(StartScanBots==0) return;
            DWORD nextBotAdress = FirstBotAdress+0xA4*static_cast<uint>(i);
            QString nextName = RMstring(nextBotAdress);
            //        DWORD nextBotGuild = FirstBotAdress+0xA4*i+0x30;
            //        QString nextGuild = RMstring(nextBotGuild);
            if(BotName.contains(nextName, Qt::CaseInsensitive)==false){
                if(nextName!=""){
                    BotName<<nextName;
                    BotLocation<<location;
                    qDebug()<<"New Bot"<<nextName<<location;
                }
            }
            else{
                for(int j=0; j<BotName.count();j++){
                    if(StartScanBots==0) return;
                    if(BotName[j]==nextName){
                        if(BotLocation[j]!=location){
                            qDebug()<<QTime::currentTime().toString("[hh:mm:ss]")
                                   <<BotName[j]
                                     <<"сменил локацию"<<BotLocation[j]
                                       <<"на"<<location;
                            ctr  = "INSERT INTO HookBots (Name,LocationPrev,LocationNext,Time) VALUES ('%1','%2','%3','%4');";
                            ctr = ctr.arg(BotName[j])
                                    .arg(BotLocation[j])
                                    .arg(location)
                                    .arg(QTime::currentTime().toString("hh:mm:ss"));
//                            qDebug()<<ctr;
                            if(!query->exec(ctr)){
                                qDebug() << query->lastError();
                                qDebug() << ctr;
                            }
                            BotLocation[j]=location;
                        }
                    }
                }
            }
        }
    }
    FirstCicle=0;
    TimerFindBots.start(200);
}

void MainWindow::on_pushScanBotsPause_clicked()
{
    if(TimerFindBots.isActive()){
        StartScanBots=0;
        TimerFindBots.stop();
        ui->pushScanBotsPause->setStyleSheet("background-color: rgb(0, 255, 0);");
    }
    else{
        StartScanBots=1;
        TimerFindBots.start(200);
        ui->pushScanBotsPause->setStyleSheet("");
    }
}

//####################### СУМКИ ################################################################


//class Container : GenericWowObject
//{
//    public: uint numberOfSlots {
//        get { return Read<uint>(BaseAddress + 0x760); }
//    };

//    public: ulong GetItemBySlot(uint slot)
//    {
//        return Read<ulong>(Read<uint>(BaseAddress + 0x3E0) + 8 + 8 * slot);
//    }
//};

//static uint BagFirstGUID = 0xC542A8;
//ulong MainWindow::GetBackpackItemGUID(uint /*slot*/){
//    if (slot > 15)
//        return 0;
//    return RM(DescriptorFields + ePlayerFields::PLAYER_FIELD_PACK_SLOT_1 * 4 + 8 * slot);
//}

//uint MainWindow::bagsFreeSlots(){
//    get {
//        uint result = 0;
//        Container bag = null;
//        for (uint i = 0; i < 16; i++)
//            if (GetBackpackItemGUID(i) == 0)
//                result++;
//        for (uint i = 0; i < 4; i++)
//        {
//            ulong bagGuid = 0;
//            bagGuid = Read<ulong>(Offsets.BagFirstGUID + i * 8);
//            if (bagGuid == 0)
//                continue;
//            bag = new Container(objectManager.GetBaseAdressByGuid(bagGuid));
//            for (uint k = 0; k < bag.numberOfSlots; k++)
//                if (bag.GetItemBySlot(k) == 0)
//                    result++;
//        }
//        return result;
//    }
//}

//static DWORD BagFirstGUID = 0xC542A8;
void MainWindow::on_pushBags_clicked()
{
//    int numberOfSlots = RMint(LocalPlayer->BaseAddress + 0x172);
//    qDebug()<<reinterpret_cast<LPCVOID>(LocalPlayer->BaseAddress)
//              <<reinterpret_cast<LPCVOID>(LocalPlayer->BaseAddress + 0x760)
//             <<"numberOfSlots"<<numberOfSlots;
    //    ui->lcdBagSlots->display(numberOfSlots);
}

void MainWindow::on_lineWaitMsLoadNode_cursorPositionChanged(int /*arg1*/, int /*arg2*/)
{
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");   //для чтения кириллицы
    QSettings settings("settings.conf", QSettings::IniFormat);
    settings.setIniCodec(codec);
    settings.setValue("Macros/WaitMsLoadNode",ui->lineWaitMsLoadNode->text());
    WaitMsLoadNode = ui->lineWaitMsLoadNode->text().toInt();
}



// О МОЙ ДБМ ЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫ
// О МОЙ ДБМ ЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫ
// О МОЙ ДБМ ЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫ
// О МОЙ ДБМ ЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫЫ


void MainWindow::on_pushJaine_clicked()
{
    TimerJaineOut();
//    if(JaineMode==false){
//        JaineMode = true;
//        qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"старт";

//        ui->pushJaine->setStyleSheet("background-color: rgb(0, 170, 0);");
//        TimerReloadAdreses->stop();
//        TimerJaine.start(1);
//    }
//    else{
//        JaineMode = false;
//        ui->pushJaine->setStyleSheet("");
//        TimerReloadAdreses->start(500);
//        TimerJaine.stop();
//    }
}



void MainWindow::DBMwrite(QString msg){
    WriteProcessMemory(hProc, reinterpret_cast<void*>(0x2E16CC18), msg.toStdString().c_str(), 100, nullptr);
}

static QString message;

void MainWindow::show_message_str(){
    WriteProcessMemory(hProc, reinterpret_cast<void*>(macros_addr),
                       message.toStdString().c_str(), 250, nullptr);
    SendKey(0x50);
}

void MainWindow::show_message(QString text){
    WriteProcessMemory(hProc, reinterpret_cast<void*>(macros_addr),
                       text.toStdString().c_str(), 250, nullptr);
    SendKey(0x50);
    qDebug() << QTime::currentTime().toString("[hh:mm:ss] ") + text;
}


int GetDistance(float XPos1, float YPos1, float XPos2, float YPos2){
    double x1 = static_cast<double>(XPos1);
    double y1 = static_cast<double>(YPos1);
    double x2 = static_cast<double>(XPos2);
    double y2 = static_cast<double>(YPos2);
    double a = x1-x2;
    double b = y1-y2;
    int dist =qPow(a,2)+qPow(b,2);
    dist=qSqrt(dist);
    return dist;
}

QString GetUglesQueueString(QVector<int> spellIDs, QVector<int> dists){

    qDebug() << "spellIDs" << spellIDs;
    qDebug() << "dists" << dists;

    int temp1,temp2;
    for (int r = 0; r < spellIDs.count() - 1; r++) {
        for (int t = 0; t < spellIDs.count() - r - 1; t++) {
            if (dists[t] > dists[t + 1]) {
                // меняем элементы местами
                temp1 = dists[t];
                dists[t] = dists[t + 1];
                dists[t + 1] = temp1;

                temp2 = spellIDs[t];
                spellIDs[t] = spellIDs[t + 1];
                spellIDs[t + 1] = temp2;
            }
        }
    }

    qDebug() << "sorted spellIDs" << spellIDs;
    qDebug() << "sorted dists" << dists;

    QString spelsQueue;
    for (int f = 0; f < spellIDs.count(); f++) {
        if(spellIDs[f] == 308663)
            spelsQueue += "ДИЗАРМ > ";
        else if(spellIDs[f] == 308664)
            spelsQueue += "ПРЕРЫВАНИЯ >";
        else if(spellIDs[f] == 308665)
            spelsQueue += "АНАГРО > ";
        else if(spellIDs[f] == 308666)
            spelsQueue += "ФИР > ";
        else if(spellIDs[f] == 308667)
            spelsQueue += "ФИР+ > ";
        else if(spellIDs[f] == 17928)
            spelsQueue += "ТестСпелл>";
    }
    return spelsQueue;
}

void autokick(int timeMS){
    delay(timeMS);
    SendKey(0x45);
}

static QTimer tim00,tim0,tim1,tim2,tim3,tim4;
static int cast1;
bool HalionYell=false;
void MainWindow::TimerJaineOut(){

    qDebug()<<QTime::currentTime().toString("[hh:mm:ss] ")+"старт";

//    qDebug() << "LocalPlayer->BaseAddress" << reinterpret_cast<LPCVOID>(LocalPlayer->BaseAddress);
//    qDebug() << GetBuffs(LocalPlayer->BaseAddress);

    while(true){

//        last_chat_msg_addr = RM(RM(RM(RM(RM(RM(0x400000+0x9CE674)+0x0)+0x98)+0x54)+0x8)+0xD4)+0x20;

        QVector<int> id, spellid, xp, yp, hp, dist;

        CurrentObject->BaseAddress = FirstObject;
        LocalPlayer->Guid = RM(ObjectManager + ClientOffsets::LocalGuidOffset);
        LocalPlayer->BaseAddress = GetObjectBaseByGuid(LocalPlayer->Guid);
        LocalTarget->Guid = RM(ClientOffsets::LocalTargetGUID);
        LocalPlayer->XPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_X);
        LocalPlayer->YPos = RMfloat(LocalPlayer->BaseAddress + ObjectOffsets::Pos_Y);

        while (CurrentObject->BaseAddress != 0 && CurrentObject->BaseAddress % 2 == 0){
            CurrentObject->Guid = RM(CurrentObject->BaseAddress + ObjectOffsets::Guid);
//            if(CurrentObject->Guid != LocalTarget->Guid){
                    CurrentObject->Type = RM(CurrentObject->BaseAddress + ObjectOffsets::Type);


                    //халион анонс луча BEGIN
//                    QString chat_msg = RMstring(last_chat_msg_addr);
//                    qDebug() << chat_msg;
//                    delay(10);
//                    if(chat_msg == "|r Халион кричит: В мире сумерек вы найдете лишь страдания! Входите, если посмеете!"
//                       ||
//                       chat_msg == "|r Халион кричит: Остерегайтесь теней!"
//                       ){
//                        if(!HalionYell){
//                            tim4.start(29 * 1000 + 5000);
//                            tim4.setObjectName(QString::number(29));
//                            HalionYell = true;
//                            qDebug() << chat_msg;
//                        }
//                    }
//                    else
//                        HalionYell = false;

//                    if(tim4.isActive()){
//                        if(tim4.remainingTime() > 10)
//                            TimerControlHalion(tim4);
//                        else if(tim4.remainingTime() <= 500)
//                            tim4.stop();
//                    }
                    //халион анонс луча END



                    if(tim1.isActive() && tim1.remainingTime() > 10) // макрос тест
                        TimerControl(tim1);
                    if(tim2.isActive() && tim2.remainingTime() > 10) // падение
                        TimerControl(tim2);


                    if(tim00.isActive() && tim00.remainingTime() <= 500) // единичный анонс очереди спелов углей
                        tim00.stop();
                    if(tim0.isActive() && tim0.remainingTime() <= 500) // единичный анонс 7% хп алара
                        tim0.stop();
                    if(tim1.isActive() && tim1.remainingTime() <= 500)
                        tim1.stop();
                    if(tim2.isActive() && tim2.remainingTime() <= 500)
                        tim2.stop();
                    if(tim3.isActive() && tim3.remainingTime() <= 500) // единичный анонс каста фениксов (углей)
                        tim3.stop();

                    /*
//            if (CurrentObject->Type == 4){ // игроки
//                CurrentObject->Guid = RM(CurrentObject->BaseAddress + ObjectOffsets::Guid);
//                CurrentObject->Name = PlayerNameFromGuid(CurrentObject->Guid);
//                if(CurrentObject->Name == "Macros"){

//                    int spellID = RMint(CurrentObject->BaseAddress+0xA6C);
//                    if(spellID == 50977 && !tim1.isActive()){
//                        show_message("/dbm broadcast timer 10 матрас ");
//                        show_message("/5 матрас тест ----");
//                        int castTimeSecs = 10;
//                        tim1.start(castTimeSecs * 1000 + 5000);
//                        tim1.setObjectName(QString::number(castTimeSecs));
//                    }
//                    else if(spellID != 50977)
//                        tim1.stop();
//                }
//                if(CurrentObject->Name == "Злоймурлок"){
//                    int hpPercent = CurrentObject->CurrentHealth / CurrentObject->MaxHealth * 100 ;
//                    if(hpPercent > 5 && hpPercent < 10 && !tim0.isActive()){
//                        tim0.start(30000);
//                        show_message(QString("/с мурлыч тест хп %1").arg(hpPercent));
//                    }
//                    delay(1000);
//                }
//            }
*/
                    //FPS СДЕЛАТЬ

                    // a npc
                    if(CurrentObject->Type==3){

                        CurrentObject->UnitFieldsAddress = RM(CurrentObject->BaseAddress + ObjectOffsets::UnitFields);
                        CurrentObject->UnitID = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::UnitID);

                        if(
//                                CurrentObject->UnitID==20064
//                                ||
//                                CurrentObject->UnitID==19622 //алар
//                                ||
                                CurrentObject->UnitID==19514 //алар
                                ||
                                CurrentObject->UnitID==19551 //алар
//                                ||
//                                CurrentObject->UnitID==19516 //робот
//                                ||
//                                CurrentObject->UnitID==18805
//                                ||
//                                CurrentObject->UnitID==200018
//                                ||
//                                CurrentObject->UnitID==200019
                                ){

                            CurrentObject->CurrentHealth = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::Health);
                            CurrentObject->MaxHealth = RM(CurrentObject->UnitFieldsAddress + UnitOffsets::MaxHealth);
                            CurrentObject->XPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Pos_X);
                            CurrentObject->YPos = RMfloat(CurrentObject->BaseAddress + ObjectOffsets::Pos_Y);


                            int id_ = CurrentObject->UnitID;
                            int spellid_ = RMint(CurrentObject->BaseAddress+0xA6C);


                            int xp_ = CurrentObject->XPos;
                            int yp_ = CurrentObject->YPos;
                            int hp_ = (double)CurrentObject->CurrentHealth / (double)CurrentObject->MaxHealth * 100;
                            int dist_ = GetDistance(LocalPlayer->XPos, LocalPlayer->YPos, CurrentObject->XPos, CurrentObject->YPos);



                            if(CurrentObject->Guid == LocalTarget->Guid && UnitID==20064){
                                if(spellid_ == 308733 || spellid_ == 308796){
                                    qDebug() << QTime::currentTime().toString("[hh:mm:ss.zzz] ") << id_ << "autokick(150);" << spellid_;
                                    autokick(150);
                                }
                            }

//                            if(CurrentObject->Guid == LocalTarget->Guid && UnitID==200018){
//                                //308561,"Высшее исцеление"
//                                if(spellid_ == 308561){
//                                    qDebug() << QTime::currentTime().toString("[hh:mm:ss.zzz] ") << id_ << "autokick(150);" << spellid_;
//                                    autokick(150);
//                                }
//                            }


                            id << id_;
                            spellid << spellid_;
                            xp << xp_;
                            yp << yp_;
                            hp << hp_;
                            dist << dist_;

//                            if(spellid_ == 0)
//                                cast1 = 0;
//                            if(cast1 == 0 && spellid_!=0){
//                                cast1 = spellid_;
//                                qDebug() << QTime::currentTime().toString("[hh:mm:ss.zzz] ") << id_ << hp_ << spellid_ << dist_;
//                            }
                        }
                    }
//                } //if(CurrentObject->Guid != LocalTarget->Guid)

            CurrentObject->BaseAddress = RM(CurrentObject->BaseAddress + 0x3C);
        }

        delay(1);


        for (int i=0;i<id.count();i++) {

//            qDebug()<<QTime::currentTime().toString("[hh:mm:ss.zzz] ") << id[i] << hp[i] << spellid[i] << dist[i];

            // АЛАР BEGIN
            if(id[i]==19514){
                if(hp[i] == 7 && !tim0.isActive()){
                    show_message("/к 7% 7% 7% 7% 7% 7%");
                    tim0.start(30000);
                }
                if(spellid[i] == 308987 && !tim2.isActive()){
//                    show_message("/dbm broadcast timer 5 ПАДЕНИЕ");
                    int castTimeSecs = 5;
                    tim2.start(castTimeSecs * 1000 + 5000);
                    show_message("/к ПАДЕНИЕ -----");
                    tim2.setObjectName(QString::number(castTimeSecs));
                }
                else if(spellid[i] != 308987){
                    tim2.stop();
                }
            }
            // АЛАР END

            // УГОЛЬ АЛАРА BEGIN
            else if(id[i]==19551){
                QVector<int> spellIDs_, dists_;
                bool readyQueue = true;
                for (int j=0;j<spellid.count();j++) {
                    if(id[j]==19551){
                        if(spellid[j]==0)
                            readyQueue = false;
                        spellIDs_ << spellid[j];
                        dists_ << dist[j];
                    }
                }
                if(readyQueue && !tim00.isActive()){
                    tim00.start(30000);
                    QString queue = GetUglesQueueString(spellIDs_, dists_);
                    if(queue.length() > 0){
                        queue.remove(queue.length()-3, 3);
                        show_message("/с "+queue);
                    }
                }

                if(dist[i] < 25){
                    if(spellid[i] != 0 && !tim3.isActive()){
                        QString spellname, dbm;
                        if(spellid[i] == 308668){
                            spellname = "ДИЗАРМ";
//                            dbm = "/dbm broadcast timer 2 ДИЗАРМ";
                            tim3.start(7000);
                        }
                        else if(spellid[i] == 308669){
                            spellname = "ПРЕРЫВАНИЕ";
//                            dbm = "/dbm broadcast timer 2 ПРЕРЫВАНИЕ";
                            tim3.start(7000);
                        }
                        else if(spellid[i] == 308670){
                            spellname = "АНАГРО";
//                            dbm = "/dbm broadcast timer 2 АНАГРО";
                            tim3.start(7000);
                        }
                        else if(spellid[i] == 308671){
                            spellname = "ФИР";
//                            dbm = "/dbm broadcast timer 2 ФИР";
                            tim3.start(7000);
                        }
                        else if(spellid[i] == 308663){
                            spellname = "РАССЕЯННОСТЬ: -80% маг.урона (ДИЗАРМ)";
//                            dbm = "/dbm broadcast timer 20 РАССЕЯННОСТЬ";
                            tim3.start(25000);
                        }
                        else if(spellid[i] == 308664){
                            spellname = "СЛАБОСТЬ: -80% физ.урона (ПРЕРЫВАНИЕ)";
//                            dbm = "/dbm broadcast timer 20 СЛАБОСТЬ";
                            tim3.start(25000);
                        }
                        else if(spellid[i] == 308665){
                            spellname = "ЯРОСТЬ: +200% агры (АНАГРО)";
//                            dbm = "/dbm broadcast timer 20 ЯРОСТЬ";
                            tim3.start(25000);
                        }
                        else if(spellid[i] == 308666){
                            spellname = "УСТАЛОСТЬ: -50% энерджи (ФИР)";
//                            dbm = "/dbm broadcast timer 20 УСТАЛОСТЬ";
                            tim3.start(25000);
                        }
                        else if(spellid[i] == 308667){
                            spellname = "УСТАЛОСТЬ+: -50% энерджи (ФИР)";
//                            dbm = "/dbm broadcast timer 20 УСТАЛОСТЬ";
                            tim3.start(25000);
                        }
                        else if(spellid[i] == 17928){
                            spellname = "ТестСпелл";
//                            dbm = "/dbm broadcast timer 2 тестспелл";
                            tim3.start(7000);
                        }
                        else if(spellid[i] == 0){
                            tim3.stop();
                        }

                        if(dbm.length() > 0)
                            show_message(dbm);
                        if(spellname.length() > 0)
                            show_message("/с "+spellname+" -----");

                    }
                }
            }
            // УГОЛЬ АЛАРА END
/*
            //РОБОТ BEGIN
            else if(id[i]==19516){
                if(spellid[i] == 308470){
                    //Тяжкий удар
                }
                else if(spellid[i] == 308984){
                    //Чародейские сферы
                }
            }
            //РОБОТ END


            //СОЛАРИАН BEGIN
            else if(id[i]==18805){

            }
            else if(id[i]==200019){ //провидец

            }
            else if(id[i]==200018){ //жрец

            }
            //СОЛАРИАН END
*/
        }
//        delay(100);
//        if(id.count()>0)
//            qDebug()<<" ----- ";

    }
}


void MainWindow::TimerControl(QTimer &timer){
    int del = timer.remainingTime() % 1000;
    int secs = timer.remainingTime() / 1000 - 5;
    if(secs < 0)
        return;

    if(del >= 0 && del < 50 && timer.objectName() != QString::number(secs)){
        if(secs > 0)
            show_message("/к "+QString::number(secs));
        timer.setObjectName(QString::number(secs));
    }
}

void MainWindow::TimerControlHalion(QTimer &timer){
    int del = timer.remainingTime() % 1000;
    int secs = timer.remainingTime() / 1000 - 5;
    if(secs < 0)
        return;

    if(del >= 0 && del < 50 && timer.objectName() != QString::number(secs)){
        if(secs < 6 && secs > 0)
            show_message("/к Лезвия через... "+QString::number(secs));
        else
            qDebug() << "таймер лезвий -"<< secs;
        timer.setObjectName(QString::number(secs));
    }
}
