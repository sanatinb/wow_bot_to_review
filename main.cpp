#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include "simplecrypt.h"

int main(int argc, char *argv[])
{
    QString FreeTrialStartDate ;

    //Set The Encryption And Decryption Key
    SimpleCrypt processSimpleCrypt(89473829);

    QString FreeTrialStartsOn("24:59:59");

    //Encrypt
    FreeTrialStartDate = processSimpleCrypt.encryptToString(FreeTrialStartsOn);
//    qDebug() << "Encrypted '"+FreeTrialStartsOn+"' to" << FreeTrialStartDate;

    //Decrypt
    QString decrypt = processSimpleCrypt.decryptToString(FreeTrialStartDate);
//    qDebug() << "Decrypted '"+FreeTrialStartDate+"' to" << decrypt;

    QApplication a(argc, argv);
    MainWindow w;
//    w.show();
    return a.exec();
}
