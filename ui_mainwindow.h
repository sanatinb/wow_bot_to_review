/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSetFilter;
    QAction *actionZeroNodes;
    QAction *actioncheckDuplicatesNodeData;
    QAction *actionqDebug_RadarTimer_Tick_STEP;
    QAction *actionStart_CheckAndCorrect_NodeNames;
    QAction *actionqDebug_CurrentNode;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tabGeneral;
    QGridLayout *gridLayout_10;
    QPushButton *StartButton;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_11;
    QPushButton *pushTPGorn;
    QPushButton *pushTPStormwind;
    QPushButton *pushTP_Dalaran;
    QPushButton *pushTPGB;
    QPushButton *pushTPZapredelie;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_4;
    QPushButton *pushCheckNPC;
    QPushButton *pushCheckPlayers;
    QPushButton *pushCheckNodes;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QPushButton *pushTimerNpc;
    QPushButton *pushTimerPlayers;
    QPushButton *pushTimerNodes;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QPushButton *MINEButton;
    QPushButton *HERBButton;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_8;
    QTableView *tableView;
    QFrame *frame_2;
    QGridLayout *gridLayout_9;
    QTableWidget *tableWidget_2;
    QPushButton *selectAllFiltr;
    QPushButton *unselectAllFiltr;
    QTextEdit *textEdit;
    QLabel *labelLooted;
    QWidget *tabDebug;
    QGridLayout *gridLayout_13;
    QCheckBox *checkBox;
    QLineEdit *lineCurCoords;
    QLabel *label_2;
    QPushButton *setPassAndLogin;
    QLineEdit *lineWaitMsLoadNode;
    QFrame *frame_4;
    QGridLayout *gridLayout_12;
    QPushButton *pushButtonXY;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_6;
    QSpinBox *ZposToMacros;
    QSpinBox *spinBoxX;
    QPushButton *ToMacros;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    QSpinBox *WileStepBox;
    QSpinBox *spinBoxY;
    QPushButton *pushBags;
    QLCDNumber *lcdBagSlots;
    QLineEdit *lineBasePlayer;
    QLabel *label;
    QCheckBox *checkReversList;
    QWidget *tab_2;
    QPushButton *pushScanBots;
    QPushButton *pushScanBotsPause;
    QWidget *tab;
    QLabel *labelGZ;
    QPushButton *pushJaine;
    QMenuBar *menuBar;
    QMenu *menuDebug;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(932, 724);
        actionSetFilter = new QAction(MainWindow);
        actionSetFilter->setObjectName(QString::fromUtf8("actionSetFilter"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/png/32x32/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSetFilter->setIcon(icon);
        actionZeroNodes = new QAction(MainWindow);
        actionZeroNodes->setObjectName(QString::fromUtf8("actionZeroNodes"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/png/32x32/grow font.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionZeroNodes->setIcon(icon1);
        actioncheckDuplicatesNodeData = new QAction(MainWindow);
        actioncheckDuplicatesNodeData->setObjectName(QString::fromUtf8("actioncheckDuplicatesNodeData"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/png/32x32/strikeout.png"), QSize(), QIcon::Normal, QIcon::Off);
        actioncheckDuplicatesNodeData->setIcon(icon2);
        actionqDebug_RadarTimer_Tick_STEP = new QAction(MainWindow);
        actionqDebug_RadarTimer_Tick_STEP->setObjectName(QString::fromUtf8("actionqDebug_RadarTimer_Tick_STEP"));
        actionqDebug_RadarTimer_Tick_STEP->setCheckable(true);
        actionqDebug_RadarTimer_Tick_STEP->setChecked(false);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/png/32x32/superscript.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionqDebug_RadarTimer_Tick_STEP->setIcon(icon3);
        actionStart_CheckAndCorrect_NodeNames = new QAction(MainWindow);
        actionStart_CheckAndCorrect_NodeNames->setObjectName(QString::fromUtf8("actionStart_CheckAndCorrect_NodeNames"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/png/32x32/book.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStart_CheckAndCorrect_NodeNames->setIcon(icon4);
        actionqDebug_CurrentNode = new QAction(MainWindow);
        actionqDebug_CurrentNode->setObjectName(QString::fromUtf8("actionqDebug_CurrentNode"));
        actionqDebug_CurrentNode->setCheckable(true);
        actionqDebug_CurrentNode->setChecked(true);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/png/32x32/up.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionqDebug_CurrentNode->setIcon(icon5);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setMaximumSize(QSize(16777215, 16777215));
        tabWidget->setElideMode(Qt::ElideLeft);
        tabWidget->setMovable(false);
        tabWidget->setTabBarAutoHide(false);
        tabGeneral = new QWidget();
        tabGeneral->setObjectName(QString::fromUtf8("tabGeneral"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabGeneral->sizePolicy().hasHeightForWidth());
        tabGeneral->setSizePolicy(sizePolicy1);
        tabGeneral->setMaximumSize(QSize(16777215, 16777215));
        gridLayout_10 = new QGridLayout(tabGeneral);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        gridLayout_10->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout_10->setVerticalSpacing(6);
        StartButton = new QPushButton(tabGeneral);
        StartButton->setObjectName(QString::fromUtf8("StartButton"));
        sizePolicy1.setHeightForWidth(StartButton->sizePolicy().hasHeightForWidth());
        StartButton->setSizePolicy(sizePolicy1);
        StartButton->setMinimumSize(QSize(0, 0));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        StartButton->setFont(font);

        gridLayout_10->addWidget(StartButton, 1, 1, 3, 1);

        groupBox_7 = new QGroupBox(tabGeneral);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        gridLayout_11 = new QGridLayout(groupBox_7);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        pushTPGorn = new QPushButton(groupBox_7);
        pushTPGorn->setObjectName(QString::fromUtf8("pushTPGorn"));

        gridLayout_11->addWidget(pushTPGorn, 2, 0, 1, 1);

        pushTPStormwind = new QPushButton(groupBox_7);
        pushTPStormwind->setObjectName(QString::fromUtf8("pushTPStormwind"));

        gridLayout_11->addWidget(pushTPStormwind, 1, 0, 1, 1);

        pushTP_Dalaran = new QPushButton(groupBox_7);
        pushTP_Dalaran->setObjectName(QString::fromUtf8("pushTP_Dalaran"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pushTP_Dalaran->sizePolicy().hasHeightForWidth());
        pushTP_Dalaran->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(pushTP_Dalaran, 0, 0, 1, 1);

        pushTPGB = new QPushButton(groupBox_7);
        pushTPGB->setObjectName(QString::fromUtf8("pushTPGB"));

        gridLayout_11->addWidget(pushTPGB, 0, 1, 1, 1);

        pushTPZapredelie = new QPushButton(groupBox_7);
        pushTPZapredelie->setObjectName(QString::fromUtf8("pushTPZapredelie"));

        gridLayout_11->addWidget(pushTPZapredelie, 1, 1, 1, 1);


        gridLayout_10->addWidget(groupBox_7, 1, 2, 3, 1);

        frame_3 = new QFrame(tabGeneral);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBox_3 = new QGroupBox(frame_3);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setAlignment(Qt::AlignCenter);
        gridLayout_4 = new QGridLayout(groupBox_3);
        gridLayout_4->setSpacing(3);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        pushCheckNPC = new QPushButton(groupBox_3);
        pushCheckNPC->setObjectName(QString::fromUtf8("pushCheckNPC"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(pushCheckNPC->sizePolicy().hasHeightForWidth());
        pushCheckNPC->setSizePolicy(sizePolicy3);
        pushCheckNPC->setMaximumSize(QSize(16777215, 34));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        pushCheckNPC->setFont(font1);

        gridLayout_4->addWidget(pushCheckNPC, 0, 0, 1, 1);

        pushCheckPlayers = new QPushButton(groupBox_3);
        pushCheckPlayers->setObjectName(QString::fromUtf8("pushCheckPlayers"));
        sizePolicy3.setHeightForWidth(pushCheckPlayers->sizePolicy().hasHeightForWidth());
        pushCheckPlayers->setSizePolicy(sizePolicy3);
        pushCheckPlayers->setMaximumSize(QSize(16777215, 34));
        pushCheckPlayers->setFont(font1);

        gridLayout_4->addWidget(pushCheckPlayers, 1, 0, 1, 1);

        pushCheckNodes = new QPushButton(groupBox_3);
        pushCheckNodes->setObjectName(QString::fromUtf8("pushCheckNodes"));
        pushCheckNodes->setEnabled(true);
        sizePolicy3.setHeightForWidth(pushCheckNodes->sizePolicy().hasHeightForWidth());
        pushCheckNodes->setSizePolicy(sizePolicy3);
        pushCheckNodes->setMaximumSize(QSize(16777215, 34));
        pushCheckNodes->setFont(font1);

        gridLayout_4->addWidget(pushCheckNodes, 2, 0, 1, 1);


        horizontalLayout_3->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(frame_3);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(110, 16777215));
        groupBox_2->setAlignment(Qt::AlignCenter);
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setSpacing(3);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        pushTimerNpc = new QPushButton(groupBox_2);
        pushTimerNpc->setObjectName(QString::fromUtf8("pushTimerNpc"));
        sizePolicy3.setHeightForWidth(pushTimerNpc->sizePolicy().hasHeightForWidth());
        pushTimerNpc->setSizePolicy(sizePolicy3);
        pushTimerNpc->setStyleSheet(QString::fromUtf8("alternate-background-color: rgba(0, 170, 0, 100);"));

        gridLayout_3->addWidget(pushTimerNpc, 0, 0, 1, 2);

        pushTimerPlayers = new QPushButton(groupBox_2);
        pushTimerPlayers->setObjectName(QString::fromUtf8("pushTimerPlayers"));
        sizePolicy3.setHeightForWidth(pushTimerPlayers->sizePolicy().hasHeightForWidth());
        pushTimerPlayers->setSizePolicy(sizePolicy3);
        pushTimerPlayers->setStyleSheet(QString::fromUtf8("alternate-background-color: rgba(0, 170, 0, 100);"));

        gridLayout_3->addWidget(pushTimerPlayers, 1, 0, 1, 2);

        pushTimerNodes = new QPushButton(groupBox_2);
        pushTimerNodes->setObjectName(QString::fromUtf8("pushTimerNodes"));
        sizePolicy3.setHeightForWidth(pushTimerNodes->sizePolicy().hasHeightForWidth());
        pushTimerNodes->setSizePolicy(sizePolicy3);
        pushTimerNodes->setMaximumSize(QSize(999999, 16777215));
        pushTimerNodes->setStyleSheet(QString::fromUtf8("alternate-background-color: rgba(0, 170, 0, 100);"));

        gridLayout_3->addWidget(pushTimerNodes, 2, 0, 1, 2);


        horizontalLayout_3->addWidget(groupBox_2);

        groupBox = new QGroupBox(frame_3);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy3.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy3);
        groupBox->setMaximumSize(QSize(100, 9999999));
        groupBox->setAlignment(Qt::AlignCenter);
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(3);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        MINEButton = new QPushButton(groupBox);
        MINEButton->setObjectName(QString::fromUtf8("MINEButton"));
        sizePolicy3.setHeightForWidth(MINEButton->sizePolicy().hasHeightForWidth());
        MINEButton->setSizePolicy(sizePolicy3);
        MINEButton->setMaximumSize(QSize(75, 16777215));

        gridLayout_2->addWidget(MINEButton, 0, 1, 1, 1);

        HERBButton = new QPushButton(groupBox);
        HERBButton->setObjectName(QString::fromUtf8("HERBButton"));
        sizePolicy3.setHeightForWidth(HERBButton->sizePolicy().hasHeightForWidth());
        HERBButton->setSizePolicy(sizePolicy3);
        HERBButton->setMaximumSize(QSize(75, 16777215));

        gridLayout_2->addWidget(HERBButton, 0, 0, 1, 1);


        horizontalLayout_3->addWidget(groupBox);


        gridLayout_10->addWidget(frame_3, 1, 0, 3, 1);

        groupBox_6 = new QGroupBox(tabGeneral);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        QSizePolicy sizePolicy4(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(groupBox_6->sizePolicy().hasHeightForWidth());
        groupBox_6->setSizePolicy(sizePolicy4);
        groupBox_6->setAlignment(Qt::AlignCenter);
        gridLayout_8 = new QGridLayout(groupBox_6);
        gridLayout_8->setSpacing(3);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(3, 3, 3, 3);
        tableView = new QTableView(groupBox_6);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setStyleSheet(QString::fromUtf8("selection-background-color: rgb(0, 170, 0, 100);"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableView->setTextElideMode(Qt::ElideMiddle);
        tableView->verticalHeader()->setVisible(false);

        gridLayout_8->addWidget(tableView, 0, 3, 1, 3);

        frame_2 = new QFrame(groupBox_6);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        QSizePolicy sizePolicy5(QSizePolicy::Maximum, QSizePolicy::Minimum);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy5);
        frame_2->setMaximumSize(QSize(400, 16777215));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_9 = new QGridLayout(frame_2);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        gridLayout_9->setContentsMargins(0, 0, 0, 0);
        tableWidget_2 = new QTableWidget(frame_2);
        if (tableWidget_2->columnCount() < 3)
            tableWidget_2->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        if (tableWidget_2->rowCount() < 1)
            tableWidget_2->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(0, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_2->setItem(0, 0, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget_2->setItem(0, 1, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget_2->setItem(0, 2, __qtablewidgetitem6);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));
        sizePolicy.setHeightForWidth(tableWidget_2->sizePolicy().hasHeightForWidth());
        tableWidget_2->setSizePolicy(sizePolicy);
        tableWidget_2->setMinimumSize(QSize(0, 0));
        tableWidget_2->setMaximumSize(QSize(9999999, 16777215));
        tableWidget_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget_2->setAlternatingRowColors(false);
        tableWidget_2->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget_2->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget_2->setShowGrid(true);
        tableWidget_2->setGridStyle(Qt::SolidLine);
        tableWidget_2->setWordWrap(true);
        tableWidget_2->setCornerButtonEnabled(true);
        tableWidget_2->setRowCount(1);
        tableWidget_2->setColumnCount(3);
        tableWidget_2->horizontalHeader()->setVisible(false);
        tableWidget_2->verticalHeader()->setVisible(false);

        gridLayout_9->addWidget(tableWidget_2, 1, 0, 1, 5);

        selectAllFiltr = new QPushButton(frame_2);
        selectAllFiltr->setObjectName(QString::fromUtf8("selectAllFiltr"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(selectAllFiltr->sizePolicy().hasHeightForWidth());
        selectAllFiltr->setSizePolicy(sizePolicy6);

        gridLayout_9->addWidget(selectAllFiltr, 0, 2, 1, 1);

        unselectAllFiltr = new QPushButton(frame_2);
        unselectAllFiltr->setObjectName(QString::fromUtf8("unselectAllFiltr"));
        sizePolicy6.setHeightForWidth(unselectAllFiltr->sizePolicy().hasHeightForWidth());
        unselectAllFiltr->setSizePolicy(sizePolicy6);

        gridLayout_9->addWidget(unselectAllFiltr, 0, 3, 1, 1);


        gridLayout_8->addWidget(frame_2, 0, 1, 11, 1);

        textEdit = new QTextEdit(groupBox_6);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        sizePolicy.setHeightForWidth(textEdit->sizePolicy().hasHeightForWidth());
        textEdit->setSizePolicy(sizePolicy);
        textEdit->setMaximumSize(QSize(16777215, 180));
        QFont font2;
        font2.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font2.setPointSize(8);
        textEdit->setFont(font2);
        textEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(0, 255, 0);"));
        textEdit->setReadOnly(true);

        gridLayout_8->addWidget(textEdit, 3, 3, 3, 2);

        labelLooted = new QLabel(groupBox_6);
        labelLooted->setObjectName(QString::fromUtf8("labelLooted"));
        QSizePolicy sizePolicy7(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(labelLooted->sizePolicy().hasHeightForWidth());
        labelLooted->setSizePolicy(sizePolicy7);

        gridLayout_8->addWidget(labelLooted, 3, 5, 3, 1);


        gridLayout_10->addWidget(groupBox_6, 4, 0, 1, 3);

        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/png/32x32/flag green.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tabGeneral, icon6, QString());
        tabDebug = new QWidget();
        tabDebug->setObjectName(QString::fromUtf8("tabDebug"));
        gridLayout_13 = new QGridLayout(tabDebug);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        checkBox = new QCheckBox(tabDebug);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        QFont font3;
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        checkBox->setFont(font3);
        checkBox->setChecked(true);

        gridLayout_13->addWidget(checkBox, 0, 1, 1, 1);

        lineCurCoords = new QLineEdit(tabDebug);
        lineCurCoords->setObjectName(QString::fromUtf8("lineCurCoords"));

        gridLayout_13->addWidget(lineCurCoords, 3, 3, 1, 2);

        label_2 = new QLabel(tabDebug);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_13->addWidget(label_2, 4, 2, 1, 1);

        setPassAndLogin = new QPushButton(tabDebug);
        setPassAndLogin->setObjectName(QString::fromUtf8("setPassAndLogin"));
        sizePolicy.setHeightForWidth(setPassAndLogin->sizePolicy().hasHeightForWidth());
        setPassAndLogin->setSizePolicy(sizePolicy);

        gridLayout_13->addWidget(setPassAndLogin, 3, 1, 1, 1);

        lineWaitMsLoadNode = new QLineEdit(tabDebug);
        lineWaitMsLoadNode->setObjectName(QString::fromUtf8("lineWaitMsLoadNode"));

        gridLayout_13->addWidget(lineWaitMsLoadNode, 4, 3, 1, 2);

        frame_4 = new QFrame(tabDebug);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        sizePolicy.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy);
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayout_12 = new QGridLayout(frame_4);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        pushButtonXY = new QPushButton(frame_4);
        pushButtonXY->setObjectName(QString::fromUtf8("pushButtonXY"));
        pushButtonXY->setEnabled(true);
        sizePolicy.setHeightForWidth(pushButtonXY->sizePolicy().hasHeightForWidth());
        pushButtonXY->setSizePolicy(sizePolicy);

        gridLayout_12->addWidget(pushButtonXY, 1, 2, 1, 1);

        groupBox_5 = new QGroupBox(frame_4);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        sizePolicy.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy);
        gridLayout_6 = new QGridLayout(groupBox_5);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        ZposToMacros = new QSpinBox(groupBox_5);
        ZposToMacros->setObjectName(QString::fromUtf8("ZposToMacros"));
        ZposToMacros->setEnabled(true);
        sizePolicy.setHeightForWidth(ZposToMacros->sizePolicy().hasHeightForWidth());
        ZposToMacros->setSizePolicy(sizePolicy);
        ZposToMacros->setMaximumSize(QSize(999999, 16777215));
        QFont font4;
        font4.setPointSize(12);
        ZposToMacros->setFont(font4);
        ZposToMacros->setMaximum(1000);
        ZposToMacros->setValue(70);

        gridLayout_6->addWidget(ZposToMacros, 0, 0, 1, 1);


        gridLayout_12->addWidget(groupBox_5, 2, 1, 1, 1);

        spinBoxX = new QSpinBox(frame_4);
        spinBoxX->setObjectName(QString::fromUtf8("spinBoxX"));
        spinBoxX->setEnabled(true);
        sizePolicy.setHeightForWidth(spinBoxX->sizePolicy().hasHeightForWidth());
        spinBoxX->setSizePolicy(sizePolicy);
        spinBoxX->setFont(font4);
        spinBoxX->setMaximum(999999);
        spinBoxX->setSingleStep(50);
        spinBoxX->setValue(520);

        gridLayout_12->addWidget(spinBoxX, 1, 0, 1, 1);

        ToMacros = new QPushButton(frame_4);
        ToMacros->setObjectName(QString::fromUtf8("ToMacros"));
        ToMacros->setEnabled(true);
        sizePolicy.setHeightForWidth(ToMacros->sizePolicy().hasHeightForWidth());
        ToMacros->setSizePolicy(sizePolicy);
        ToMacros->setMaximumSize(QSize(85, 16777215));

        gridLayout_12->addWidget(ToMacros, 2, 0, 1, 1);

        groupBox_4 = new QGroupBox(frame_4);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        sizePolicy.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy);
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        WileStepBox = new QSpinBox(groupBox_4);
        WileStepBox->setObjectName(QString::fromUtf8("WileStepBox"));
        WileStepBox->setEnabled(true);
        sizePolicy.setHeightForWidth(WileStepBox->sizePolicy().hasHeightForWidth());
        WileStepBox->setSizePolicy(sizePolicy);
        WileStepBox->setFont(font4);
        WileStepBox->setMinimum(10);
        WileStepBox->setMaximum(1000);
        WileStepBox->setSingleStep(10);
        WileStepBox->setValue(500);

        gridLayout_5->addWidget(WileStepBox, 0, 0, 1, 1);


        gridLayout_12->addWidget(groupBox_4, 3, 0, 1, 3);

        spinBoxY = new QSpinBox(frame_4);
        spinBoxY->setObjectName(QString::fromUtf8("spinBoxY"));
        spinBoxY->setEnabled(true);
        sizePolicy.setHeightForWidth(spinBoxY->sizePolicy().hasHeightForWidth());
        spinBoxY->setSizePolicy(sizePolicy);
        spinBoxY->setFont(font4);
        spinBoxY->setMaximum(999999);
        spinBoxY->setSingleStep(50);
        spinBoxY->setValue(403);

        gridLayout_12->addWidget(spinBoxY, 1, 1, 1, 1);


        gridLayout_13->addWidget(frame_4, 6, 1, 1, 1);

        pushBags = new QPushButton(tabDebug);
        pushBags->setObjectName(QString::fromUtf8("pushBags"));
        QSizePolicy sizePolicy8(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(pushBags->sizePolicy().hasHeightForWidth());
        pushBags->setSizePolicy(sizePolicy8);

        gridLayout_13->addWidget(pushBags, 2, 1, 1, 1);

        lcdBagSlots = new QLCDNumber(tabDebug);
        lcdBagSlots->setObjectName(QString::fromUtf8("lcdBagSlots"));
        lcdBagSlots->setDigitCount(3);

        gridLayout_13->addWidget(lcdBagSlots, 2, 2, 1, 1);

        lineBasePlayer = new QLineEdit(tabDebug);
        lineBasePlayer->setObjectName(QString::fromUtf8("lineBasePlayer"));
        sizePolicy.setHeightForWidth(lineBasePlayer->sizePolicy().hasHeightForWidth());
        lineBasePlayer->setSizePolicy(sizePolicy);

        gridLayout_13->addWidget(lineBasePlayer, 4, 1, 1, 1);

        label = new QLabel(tabDebug);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_13->addWidget(label, 3, 2, 1, 1);

        checkReversList = new QCheckBox(tabDebug);
        checkReversList->setObjectName(QString::fromUtf8("checkReversList"));
        checkReversList->setFont(font3);
        checkReversList->setChecked(true);

        gridLayout_13->addWidget(checkReversList, 1, 1, 1, 1);

        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/png/32x32/flag red.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(tabDebug, icon7, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        pushScanBots = new QPushButton(tab_2);
        pushScanBots->setObjectName(QString::fromUtf8("pushScanBots"));
        pushScanBots->setGeometry(QRect(10, 10, 171, 81));
        pushScanBotsPause = new QPushButton(tab_2);
        pushScanBotsPause->setObjectName(QString::fromUtf8("pushScanBotsPause"));
        pushScanBotsPause->setGeometry(QRect(10, 120, 171, 61));
        tabWidget->addTab(tab_2, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        labelGZ = new QLabel(tab);
        labelGZ->setObjectName(QString::fromUtf8("labelGZ"));
        labelGZ->setGeometry(QRect(10, 70, 201, 71));
        QFont font5;
        font5.setPointSize(14);
        labelGZ->setFont(font5);
        labelGZ->setFrameShape(QFrame::Box);
        labelGZ->setAlignment(Qt::AlignCenter);
        pushJaine = new QPushButton(tab);
        pushJaine->setObjectName(QString::fromUtf8("pushJaine"));
        pushJaine->setGeometry(QRect(10, 10, 111, 23));
        tabWidget->addTab(tab, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 932, 21));
        menuDebug = new QMenu(menuBar);
        menuDebug->setObjectName(QString::fromUtf8("menuDebug"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        QSizePolicy sizePolicy9(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy9.setHorizontalStretch(0);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(mainToolBar->sizePolicy().hasHeightForWidth());
        mainToolBar->setSizePolicy(sizePolicy9);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuDebug->menuAction());
        menuDebug->addAction(actionqDebug_CurrentNode);
        menuDebug->addAction(actionqDebug_RadarTimer_Tick_STEP);
        menuDebug->addAction(actioncheckDuplicatesNodeData);
        menuDebug->addAction(actionStart_CheckAndCorrect_NodeNames);
        mainToolBar->addAction(actionSetFilter);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionZeroNodes);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "svchost", nullptr));
        actionSetFilter->setText(QCoreApplication::translate("MainWindow", "\320\244\320\270\320\273\321\214\321\202\321\200", nullptr));
#if QT_CONFIG(tooltip)
        actionSetFilter->setToolTip(QCoreApplication::translate("MainWindow", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214 \321\207\321\202\320\276 \320\276\321\202\320\276\320\261\321\200\320\260\320\266\320\260\321\202\321\214 (\321\202\321\200\320\260\320\262\320\260/\321\200\321\203\320\264\320\260)", nullptr));
#endif // QT_CONFIG(tooltip)
        actionZeroNodes->setText(QCoreApplication::translate("MainWindow", "\320\236\320\261\320\275\321\203\320\273\320\270\321\202\321\214 \321\202\320\276\321\207\320\272\320\270", nullptr));
#if QT_CONFIG(tooltip)
        actionZeroNodes->setToolTip(QCoreApplication::translate("MainWindow", "\320\236\320\261\320\275\321\203\320\273\320\270\321\202\321\214 LastUse (\320\276\321\202\321\201\320\276\321\200\321\202\320\270\321\200\321\203\320\265\321\202\321\201\321\217 \320\277\320\276 \320\273\320\276\320\272\320\260\321\206\320\270\321\217\320\274)", nullptr));
#endif // QT_CONFIG(tooltip)
        actioncheckDuplicatesNodeData->setText(QCoreApplication::translate("MainWindow", "checkDuplicatesNodeData()", nullptr));
        actionqDebug_RadarTimer_Tick_STEP->setText(QCoreApplication::translate("MainWindow", "qDebug_RadarTimer_Tick STEP", nullptr));
        actionStart_CheckAndCorrect_NodeNames->setText(QCoreApplication::translate("MainWindow", "Start_CheckAndCorrect_NodeNames", nullptr));
        actionqDebug_CurrentNode->setText(QCoreApplication::translate("MainWindow", "qDebug_CurrentNode", nullptr));
        StartButton->setText(QCoreApplication::translate("MainWindow", "START", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("MainWindow", "\320\242\320\265\320\273\320\265\320\277\320\276\321\200\321\202\321\213\321\207\320\270", nullptr));
        pushTPGorn->setText(QCoreApplication::translate("MainWindow", "\320\223\320\276\321\200\320\275", nullptr));
        pushTPStormwind->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\320\276\320\273\320\270\321\206\320\260", nullptr));
        pushTP_Dalaran->setText(QCoreApplication::translate("MainWindow", "\320\224\320\260\320\273\320\260\321\200\320\260\320\275", nullptr));
        pushTPGB->setText(QCoreApplication::translate("MainWindow", "\320\223\320\221", nullptr));
        pushTPZapredelie->setText(QCoreApplication::translate("MainWindow", "\320\227\320\260\320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\265", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "\320\241\320\272\320\260\320\275 \321\200\320\260\320\264\320\260\321\200\320\276\320\274", nullptr));
        pushCheckNPC->setText(QCoreApplication::translate("MainWindow", "NPC", nullptr));
        pushCheckPlayers->setText(QCoreApplication::translate("MainWindow", "Players", nullptr));
        pushCheckNodes->setText(QCoreApplication::translate("MainWindow", "Nodes", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "\320\227\320\260\321\206\320\270\320\272\320\273\320\270\321\202\321\214 \321\200\320\260\320\264\320\260\321\200", nullptr));
        pushTimerNpc->setText(QCoreApplication::translate("MainWindow", "While Npc", nullptr));
        pushTimerPlayers->setText(QCoreApplication::translate("MainWindow", "While Players", nullptr));
        pushTimerNodes->setText(QCoreApplication::translate("MainWindow", "While Nodes", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "\320\244\320\270\320\273\321\214\321\202\321\200\321\213", nullptr));
        MINEButton->setText(QCoreApplication::translate("MainWindow", "\320\240\320\243\320\224\320\220", nullptr));
        HERBButton->setText(QCoreApplication::translate("MainWindow", "\320\242\320\240\320\220\320\222\320\220", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("MainWindow", "Nodes in table - 0", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("MainWindow", "\320\230\320\274\321\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("MainWindow", "\320\235\320\260\320\262\321\213\320\272", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("MainWindow", "state", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_2->verticalHeaderItem(0);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("MainWindow", "123dsdaed", nullptr));

        const bool __sortingEnabled = tableWidget_2->isSortingEnabled();
        tableWidget_2->setSortingEnabled(false);
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_2->item(0, 0);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("MainWindow", "12312c1def", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget_2->item(0, 1);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("MainWindow", "sefwe", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget_2->item(0, 2);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("MainWindow", "fwefwef", nullptr));
        tableWidget_2->setSortingEnabled(__sortingEnabled);

        selectAllFiltr->setText(QCoreApplication::translate("MainWindow", "\320\262\321\213\320\264\320\265\320\273\320\270\321\202\321\214 \320\262\321\201\320\265", nullptr));
        unselectAllFiltr->setText(QCoreApplication::translate("MainWindow", "\321\201\320\275\321\217\321\202\321\214 \320\262\321\201\320\265", nullptr));
        textEdit->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", nullptr));
        labelLooted->setText(QCoreApplication::translate("MainWindow", "labelLooted", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabGeneral), QCoreApplication::translate("MainWindow", "General", nullptr));
        checkBox->setText(QCoreApplication::translate("MainWindow", "\320\230\320\263\320\275\320\276\321\200\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \321\202\320\276\321\207\320\272\320\270 \320\263\320\264\320\265 \321\207\320\260\321\201\321\202\320\276 \321\203\320\274\320\270\321\200\320\260\320\265\321\210\321\214 (>5 \321\200\320\260\320\267)", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "WaitMsLoadNode", nullptr));
        setPassAndLogin->setText(QCoreApplication::translate("MainWindow", "setPassAndLogin()", nullptr));
        pushButtonXY->setText(QCoreApplication::translate("MainWindow", "MouseTest", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("MainWindow", "-Z \320\277\320\276\320\264 \320\274\320\260\321\202\321\200\320\260\321\201\320\276\320\274", nullptr));
        ToMacros->setText(QCoreApplication::translate("MainWindow", "ToMacros Mode\n"
"+ Fly", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "\320\270\320\275\321\202\320\265\321\200\320\262\320\260\320\273 \320\267\320\260\321\206\320\270\320\272\320\273.", nullptr));
        pushBags->setText(QCoreApplication::translate("MainWindow", "\320\247\320\225\320\232 \320\241\320\243\320\234\320\232\320\230 pushBags", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "\320\222\320\260\321\210\320\270 \320\272\320\276\320\276\321\200\320\264\320\270\320\275\320\260\321\202\321\213", nullptr));
        checkReversList->setText(QCoreApplication::translate("MainWindow", "\320\236\320\261\321\200\320\260\321\202\320\275\320\260\321\217 \321\201\320\276\321\200\321\202\320\270\321\200\320\276\320\262\320\272\320\260 \320\277\320\276\321\201\320\273\320\265 \320\277\321\200\320\276\321\205\320\276\320\266\320\264\320\265\320\275\320\270\321\217 \321\201\320\277\320\270\321\201\320\272\320\260", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabDebug), QCoreApplication::translate("MainWindow", "Debug", nullptr));
        pushScanBots->setText(QCoreApplication::translate("MainWindow", "Hook Bots", nullptr));
        pushScanBotsPause->setText(QCoreApplication::translate("MainWindow", "Pause", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "HookBots", nullptr));
        labelGZ->setText(QCoreApplication::translate("MainWindow", "\320\223\320\276\321\200\321\217\321\211\320\260\321\217 \320\267\320\265\320\274\320\273\321\217", nullptr));
        pushJaine->setText(QCoreApplication::translate("MainWindow", "\320\222\320\272\320\273\321\216\321\207\320\270\321\202\321\214 \320\264\320\266\320\260\320\271\320\275\321\203", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "\320\224\320\266\320\260\320\271\320\275\320\260", nullptr));
        menuDebug->setTitle(QCoreApplication::translate("MainWindow", "Debug", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
