#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <wowobject.h>
#include "windows.h"
#include "QtMath"
#include "QTimer"
#include <QSqlTableModel>
#include <QLabel>
#include "QSettings"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    enum ClientOffsets : uint
    {
        StaticClientConnection = 0x00C79CE0,
        ObjectManagerOffset = 0x2ED0,
        FirstObjectOffset = 0xAC,
        LocalGuidOffset = 0xC0,
        NextObjectOffset = 0x3C,
        LocalPlayerGUID = 0xBD07A8,
        LocalTargetGUID = 0x00BD07B0,
        CurrentContinent = 0x00ACCF04
    };

    enum NameOffsets : ulong
    {
        nameStore = 0x00C5D938 + 0x8,
        nameMask = 0x24,
        nameBase = 0x1C,
        nameString = 0x20,
        PlayerName = 0x8BF1E0, //0x8B2FB0,
        mobName = 0xA24, //good
        mobNameEx = 0x60, //good
        continentName = 0x9162A0,
        continentNameEx = 0x28DC
    };

    enum ObjectOffsets : uint
    {
        Type = 0x14,
        Pos_X = 0x79C,
        Pos_Y = 0x798,
        Pos_Z = 0x7A0,
        Rot = 0x7A8,
        Guid = 0x30,
        UnitFields = 0x8,
        Node_Pos_X = 0xEC,
        Node_Pos_Y = 0xE8,
        Node_Pos_Z = 0xF0,
        DisplayID = 0x20,
        ObjectID = 0xEC+0x138
    };

    enum ePlayerFields
    {
       PLAYER_DUEL_ARBITER = 0x94,
       PLAYER_FLAGS = 0x96,
       PLAYER_GUILDID = 0x97,
       PLAYER_GUILDRANK = 0x98,
       PLAYER_BYTES = 0x99,
       PLAYER_BYTES_2 = 0x9A,
       PLAYER_BYTES_3 = 0x9B,
       PLAYER_DUEL_TEAM = 0x9C,
       PLAYER_GUILD_TIMESTAMP = 0x9D,
       PLAYER_QUEST_LOG_1_1 = 0x9E,
       PLAYER_QUEST_LOG_1_2 = 0x9F,
       PLAYER_QUEST_LOG_1_3 = 0xA0,
       PLAYER_QUEST_LOG_1_4 = 0xA2,
       PLAYER_QUEST_LOG_2_1 = 0xA3,
       PLAYER_QUEST_LOG_2_2 = 0xA4,
       PLAYER_QUEST_LOG_2_3 = 0xA5,
       PLAYER_QUEST_LOG_2_5 = 0xA7,
       PLAYER_QUEST_LOG_3_1 = 0xA8,
       PLAYER_QUEST_LOG_3_2 = 0xA9,
       PLAYER_QUEST_LOG_3_3 = 0xAA,
       PLAYER_QUEST_LOG_3_5 = 0xAC,
       PLAYER_QUEST_LOG_4_1 = 0xAD,
       PLAYER_QUEST_LOG_4_2 = 0xAE,
       PLAYER_QUEST_LOG_4_3 = 0xAF,
       PLAYER_QUEST_LOG_4_5 = 0xB1,
       PLAYER_QUEST_LOG_5_1 = 0xB2,
       PLAYER_QUEST_LOG_5_2 = 0xB3,
       PLAYER_QUEST_LOG_5_3 = 0xB4,
       PLAYER_QUEST_LOG_5_5 = 0xB6,
       PLAYER_QUEST_LOG_6_1 = 0xB7,
       PLAYER_QUEST_LOG_6_2 = 0xB8,
       PLAYER_QUEST_LOG_6_3 = 0xB9,
       PLAYER_QUEST_LOG_6_5 = 0xBB,
       PLAYER_QUEST_LOG_7_1 = 0xBC,
       PLAYER_QUEST_LOG_7_2 = 0xBD,
       PLAYER_QUEST_LOG_7_3 = 0xBE,
       PLAYER_QUEST_LOG_7_5 = 0xC0,
       PLAYER_QUEST_LOG_8_1 = 0xC1,
       PLAYER_QUEST_LOG_8_2 = 0xC2,
       PLAYER_QUEST_LOG_8_3 = 0xC3,
       PLAYER_QUEST_LOG_8_5 = 0xC5,
       PLAYER_QUEST_LOG_9_1 = 0xC6,
       PLAYER_QUEST_LOG_9_2 = 0xC7,
       PLAYER_QUEST_LOG_9_3 = 0xC8,
       PLAYER_QUEST_LOG_9_5 = 0xCA,
       PLAYER_QUEST_LOG_10_1 = 0xCB,
       PLAYER_QUEST_LOG_10_2 = 0xCC,
       PLAYER_QUEST_LOG_10_3 = 0xCD,
       PLAYER_QUEST_LOG_10_5 = 0xCF,
       PLAYER_QUEST_LOG_11_1 = 0xD0,
       PLAYER_QUEST_LOG_11_2 = 0xD1,
       PLAYER_QUEST_LOG_11_3 = 0xD2,
       PLAYER_QUEST_LOG_11_5 = 0xD4,
       PLAYER_QUEST_LOG_12_1 = 0xD5,
       PLAYER_QUEST_LOG_12_2 = 0xD6,
       PLAYER_QUEST_LOG_12_3 = 0xD7,
       PLAYER_QUEST_LOG_12_5 = 0xD9,
       PLAYER_QUEST_LOG_13_1 = 0xDA,
       PLAYER_QUEST_LOG_13_2 = 0xDB,
       PLAYER_QUEST_LOG_13_3 = 0xDC,
       PLAYER_QUEST_LOG_13_5 = 0xDE,
       PLAYER_QUEST_LOG_14_1 = 0xDF,
       PLAYER_QUEST_LOG_14_2 = 0xE0,
       PLAYER_QUEST_LOG_14_3 = 0xE1,
       PLAYER_QUEST_LOG_14_5 = 0xE3,
       PLAYER_QUEST_LOG_15_1 = 0xE4,
       PLAYER_QUEST_LOG_15_2 = 0xE5,
       PLAYER_QUEST_LOG_15_3 = 0xE6,
       PLAYER_QUEST_LOG_15_5 = 0xE8,
       PLAYER_QUEST_LOG_16_1 = 0xE9,
       PLAYER_QUEST_LOG_16_2 = 0xEA,
       PLAYER_QUEST_LOG_16_3 = 0xEB,
       PLAYER_QUEST_LOG_16_5 = 0xED,
       PLAYER_QUEST_LOG_17_1 = 0xEE,
       PLAYER_QUEST_LOG_17_2 = 0xEF,
       PLAYER_QUEST_LOG_17_3 = 0xF0,
       PLAYER_QUEST_LOG_17_5 = 0xF2,
       PLAYER_QUEST_LOG_18_1 = 0xF3,
       PLAYER_QUEST_LOG_18_2 = 0xF4,
       PLAYER_QUEST_LOG_18_3 = 0xF5,
       PLAYER_QUEST_LOG_18_5 = 0xF7,
       PLAYER_QUEST_LOG_19_1 = 0xF8,
       PLAYER_QUEST_LOG_19_2 = 0xF9,
       PLAYER_QUEST_LOG_19_3 = 0xFA,
       PLAYER_QUEST_LOG_19_5 = 0xFC,
       PLAYER_QUEST_LOG_20_1 = 0xFD,
       PLAYER_QUEST_LOG_20_2 = 0xFE,
       PLAYER_QUEST_LOG_20_3 = 0xFF,
       PLAYER_QUEST_LOG_20_5 = 0x101,
       PLAYER_QUEST_LOG_21_1 = 0x102,
       PLAYER_QUEST_LOG_21_2 = 0x103,
       PLAYER_QUEST_LOG_21_3 = 0x104,
       PLAYER_QUEST_LOG_21_5 = 0x106,
       PLAYER_QUEST_LOG_22_1 = 0x107,
       PLAYER_QUEST_LOG_22_2 = 0x108,
       PLAYER_QUEST_LOG_22_3 = 0x109,
       PLAYER_QUEST_LOG_22_5 = 0x10B,
       PLAYER_QUEST_LOG_23_1 = 0x10C,
       PLAYER_QUEST_LOG_23_2 = 0x10D,
       PLAYER_QUEST_LOG_23_3 = 0x10E,
       PLAYER_QUEST_LOG_23_5 = 0x110,
       PLAYER_QUEST_LOG_24_1 = 0x111,
       PLAYER_QUEST_LOG_24_2 = 0x112,
       PLAYER_QUEST_LOG_24_3 = 0x113,
       PLAYER_QUEST_LOG_24_5 = 0x115,
       PLAYER_QUEST_LOG_25_1 = 0x116,
       PLAYER_QUEST_LOG_25_2 = 0x117,
       PLAYER_QUEST_LOG_25_3 = 0x118,
       PLAYER_QUEST_LOG_25_5 = 0x11A,
       PLAYER_VISIBLE_ITEM_1_ENTRYID = 0x11B,
       PLAYER_VISIBLE_ITEM_1_ENCHANTMENT = 0x11C,
       PLAYER_VISIBLE_ITEM_2_ENTRYID = 0x11D,
       PLAYER_VISIBLE_ITEM_2_ENCHANTMENT = 0x11E,
       PLAYER_VISIBLE_ITEM_3_ENTRYID = 0x11F,
       PLAYER_VISIBLE_ITEM_3_ENCHANTMENT = 0x120,
       PLAYER_VISIBLE_ITEM_4_ENTRYID = 0x121,
       PLAYER_VISIBLE_ITEM_4_ENCHANTMENT = 0x122,
       PLAYER_VISIBLE_ITEM_5_ENTRYID = 0x123,
       PLAYER_VISIBLE_ITEM_5_ENCHANTMENT = 0x124,
       PLAYER_VISIBLE_ITEM_6_ENTRYID = 0x125,
       PLAYER_VISIBLE_ITEM_6_ENCHANTMENT = 0x126,
       PLAYER_VISIBLE_ITEM_7_ENTRYID = 0x127,
       PLAYER_VISIBLE_ITEM_7_ENCHANTMENT = 0x128,
       PLAYER_VISIBLE_ITEM_8_ENTRYID = 0x129,
       PLAYER_VISIBLE_ITEM_8_ENCHANTMENT = 0x12A,
       PLAYER_VISIBLE_ITEM_9_ENTRYID = 0x12B,
       PLAYER_VISIBLE_ITEM_9_ENCHANTMENT = 0x12C,
       PLAYER_VISIBLE_ITEM_10_ENTRYID = 0x12D,
       PLAYER_VISIBLE_ITEM_10_ENCHANTMENT = 0x12E,
       PLAYER_VISIBLE_ITEM_11_ENTRYID = 0x12F,
       PLAYER_VISIBLE_ITEM_11_ENCHANTMENT = 0x130,
       PLAYER_VISIBLE_ITEM_12_ENTRYID = 0x131,
       PLAYER_VISIBLE_ITEM_12_ENCHANTMENT = 0x132,
       PLAYER_VISIBLE_ITEM_13_ENTRYID = 0x133,
       PLAYER_VISIBLE_ITEM_13_ENCHANTMENT = 0x134,
       PLAYER_VISIBLE_ITEM_14_ENTRYID = 0x135,
       PLAYER_VISIBLE_ITEM_14_ENCHANTMENT = 0x136,
       PLAYER_VISIBLE_ITEM_15_ENTRYID = 0x137,
       PLAYER_VISIBLE_ITEM_15_ENCHANTMENT = 0x138,
       PLAYER_VISIBLE_ITEM_16_ENTRYID = 0x139,
       PLAYER_VISIBLE_ITEM_16_ENCHANTMENT = 0x13A,
       PLAYER_VISIBLE_ITEM_17_ENTRYID = 0x13B,
       PLAYER_VISIBLE_ITEM_17_ENCHANTMENT = 0x13C,
       PLAYER_VISIBLE_ITEM_18_ENTRYID = 0x13D,
       PLAYER_VISIBLE_ITEM_18_ENCHANTMENT = 0x13E,
       PLAYER_VISIBLE_ITEM_19_ENTRYID = 0x13F,
       PLAYER_VISIBLE_ITEM_19_ENCHANTMENT = 0x140,
       PLAYER_CHOSEN_TITLE = 0x141,
       PLAYER_FAKE_INEBRIATION = 0x142,
       PLAYER_FIELD_PAD_0 = 0x143,
       PLAYER_FIELD_INV_SLOT_HEAD = 0x144,
       PLAYER_FIELD_PACK_SLOT_1 = 0x172,
       PLAYER_FIELD_BANK_SLOT_1 = 0x192,
       PLAYER_FIELD_BANKBAG_SLOT_1 = 0x1CA,
       PLAYER_FIELD_VENDORBUYBACK_SLOT_1 = 0x1D8,
       PLAYER_FIELD_KEYRING_SLOT_1 = 0x1F0,
       PLAYER_FIELD_CURRENCYTOKEN_SLOT_1 = 0x230,
       PLAYER_FARSIGHT = 0x270,
       PLAYER__FIELD_KNOWN_TITLES = 0x272,
       PLAYER__FIELD_KNOWN_TITLES1 = 0x274,
       PLAYER__FIELD_KNOWN_TITLES2 = 0x276,
       PLAYER_FIELD_KNOWN_CURRENCIES = 0x278,
       PLAYER_XP = 0x27A,
       PLAYER_NEXT_LEVEL_XP = 0x27B,
       PLAYER_SKILL_INFO_1_1 = 0x27C,
       PLAYER_CHARACTER_POINTS1 = 0x3FC,
       PLAYER_CHARACTER_POINTS2 = 0x3FD,
       PLAYER_TRACK_CREATURES = 0x3FE,
       PLAYER_TRACK_RESOURCES = 0x3FF,
       PLAYER_BLOCK_PERCENTAGE = 0x400,
       PLAYER_DODGE_PERCENTAGE = 0x401,
       PLAYER_PARRY_PERCENTAGE = 0x402,
       PLAYER_EXPERTISE = 0x403,
       PLAYER_OFFHAND_EXPERTISE = 0x404,
       PLAYER_CRIT_PERCENTAGE = 0x405,
       PLAYER_RANGED_CRIT_PERCENTAGE = 0x406,
       PLAYER_OFFHAND_CRIT_PERCENTAGE = 0x407,
       PLAYER_SPELL_CRIT_PERCENTAGE1 = 0x408,
       PLAYER_SHIELD_BLOCK = 0x40F,
       PLAYER_SHIELD_BLOCK_CRIT_PERCENTAGE = 0x410,
       PLAYER_EXPLORED_ZONES_1 = 0x411,
       PLAYER_REST_STATE_EXPERIENCE = 0x491,
       PLAYER_FIELD_COINAGE = 0x492,
       PLAYER_FIELD_MOD_DAMAGE_DONE_POS = 0x493,
       PLAYER_FIELD_MOD_DAMAGE_DONE_NEG = 0x49A,
       PLAYER_FIELD_MOD_DAMAGE_DONE_PCT = 0x4A1,
       PLAYER_FIELD_MOD_HEALING_DONE_POS = 0x4A8,
       PLAYER_FIELD_MOD_HEALING_PCT = 0x4A9,
       PLAYER_FIELD_MOD_HEALING_DONE_PCT = 0x4AA,
       PLAYER_FIELD_MOD_TARGET_RESISTANCE = 0x4AB,
       PLAYER_FIELD_MOD_TARGET_PHYSICAL_RESISTANCE = 0x4AC,
       PLAYER_FIELD_BYTES = 0x4AD,
       PLAYER_AMMO_ID = 0x4AE,
       PLAYER_SELF_RES_SPELL = 0x4AF,
       PLAYER_FIELD_PVP_MEDALS = 0x4B0,
       PLAYER_FIELD_BUYBACK_PRICE_1 = 0x4B1,
       PLAYER_FIELD_BUYBACK_TIMESTAMP_1 = 0x4BD,
       PLAYER_FIELD_KILLS = 0x4C9,
       PLAYER_FIELD_TODAY_CONTRIBUTION = 0x4CA,
       PLAYER_FIELD_YESTERDAY_CONTRIBUTION = 0x4CB,
       PLAYER_FIELD_LIFETIME_HONORBALE_KILLS = 0x4CC,
       PLAYER_FIELD_BYTES2 = 0x4CD,
       PLAYER_FIELD_WATCHED_FACTION_INDEX = 0x4CE,
       PLAYER_FIELD_COMBAT_RATING_1 = 0x4CF,
       PLAYER_FIELD_ARENA_TEAM_INFO_1_1 = 0x4E8,
       PLAYER_FIELD_HONOR_CURRENCY = 0x4FD,
       PLAYER_FIELD_ARENA_CURRENCY = 0x4FE,
       PLAYER_FIELD_MAX_LEVEL = 0x4FF,
       PLAYER_FIELD_DAILY_QUESTS_1 = 0x500,
       PLAYER_RUNE_REGEN_1 = 0x519,
       PLAYER_NO_REAGENT_COST_1 = 0x51D,
       PLAYER_FIELD_GLYPH_SLOTS_1 = 0x520,
       PLAYER_FIELD_GLYPHS_1 = 0x526,
       PLAYER_GLYPHS_ENABLED = 0x52C,
       PLAYER_PET_SPELL_POWER = 0x52D,
       //TOTAL_PLAYER_FIELDS = 0xD7
    };

    enum UnitOffsets : uint
    {
        Name = 0x6B3320,
        Spell_C_CastSpell = 0x0080DA40,
        Level = 0x36 * 4,
        UnitID = 0xC,
        PlayerID = 0x60,
        Health = 0x18 * 4,
        Energy = 0x19 * 4,
        MaxHealth = 0x20 * 4,
        SummonedBy = 0xE * 4,
        MaxEnergy = 0x21 * 4,
        Faction = 0x37 *4,
        IsCasting = 0xA6C,
        ChanneledCasting = 0xA80,
    };

    enum UnitBaseGetUnitAura
    {
        AURA_COUNT_1 = 0xDD0,                       // 3.3.5a 12340
        AURA_COUNT_2 = 0xC54,                       // 3.3.5a 12340
        AURA_TABLE_1 = 0xC50,                       // 3.3.5a 12340
        AURA_TABLE_2 = 0xC58,                       // 3.3.5a 12340
        AURA_SIZE = 0x18,                           // 3.3.5a 12340
        AURA_SPELL_ID = 0x8                         // 3.3.5a 12340
    };

    enum WoWGameObjectType : uint
    {
        Door = 0,
        Button = 1,
        QuestGiver = 2,
        Chest = 3,
        Binder = 4,
        Generic = 5,
        Trap = 6,
        Chair = 7,
        SpellFocus = 8,
        Text = 9,
        Goober = 0xa,
        Transport = 0xb,
        AreaDamage = 0xc,
        Camera = 0xd,
        WorldObj = 0xe,
        MapObjTransport = 0xf,
        DuelArbiter = 0x10,
        FishingNode = 0x11,
        Ritual = 0x12,
        Mailbox = 0x13,
        AuctionHouse = 0x14,
        SpellCaster = 0x16,
        MeetingStone = 0x17,
        Unkown18 = 0x18,
        FishingPool = 0x19,
        FORCEDWORD = 0xFFFFFFFF,
    };


public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void RadarTimer_Tick(QString);

public slots:
    uint GetObjectBaseByGuid(ulong Guid);
    float RadianToDegree(float Rotation);
    QString MobNameFromGuid(ulong Guid);
    QString PlayerNameFromGuid(ulong guid);
    ulong GetObjectGuidByBase(uint Base);
    bool LoadAddresses();
    void TimerOut();
    void TimerReloadAdresesOut();
    void keyPressEvent (QKeyEvent*);

    QStringList setFiltrTableToSqlStr();
    QVector<int> GetBuffs(DWORD curObj);
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
    void TeleportTo(float X, float Y, float Z);
    void TeleportToDeathPoint();

//    ulong GetBackpackItemGUID(uint slot);
//    uint bagsFreeSlots();

    void TeleportToD(double X, double Y, double Z);
    void show_message_str();
    void TimerControl(QTimer &);

protected slots:
    void resetTable();
private slots:
    void on_pushCheckNPC_clicked();
    void on_pushCheckPlayers_clicked();
    void on_pushCheckNodes_clicked();
    void on_pushTimerNodes_clicked();
    void on_pushTimerNpc_clicked();
    void on_pushTimerPlayers_clicked();
    void on_ToMacros_clicked();
    void on_ZposToMacros_valueChanged(int arg1);
    void on_WileStepBox_valueChanged(int arg1);
    void on_HERBButton_clicked();
    void on_MINEButton_clicked();
    void on_StartButton_clicked();
    void on_pushButtonXY_clicked();
    void stopFarmF1();
    void on_actionSetFilter_triggered();
    void updateFiltrTable();
    void on_tableWidget_2_cellClicked(int row, int column);
    void on_selectAllFiltr_clicked();
    void on_unselectAllFiltr_clicked();
    void on_tableWidget_2_cellActivated(int row, int column);
    void updateSQLModelTable(QString Mode);
    void on_actionZeroNodes_triggered();
    void on_tableView_activated(const QModelIndex &index);
    void on_actioncheckDuplicatesNodeData_triggered();

    void on_actionqDebug_RadarTimer_Tick_STEP_triggered();

    void on_actionStart_CheckAndCorrect_NodeNames_triggered();

    void on_actionqDebug_CurrentNode_triggered();

    void on_pushTP_Dalaran_clicked();
    void on_pushTPStormwind_clicked();
    void on_pushTPZapredelie_clicked();
    void on_pushTPGB_clicked();
    void on_pushTPGorn_clicked();

    bool setPassAndLogin();
    void on_setPassAndLogin_clicked();

    bool IsLoadingOrConnecting();

    void on_lineCurCoords_returnPressed();


    void on_pushJaine_clicked();

    void TimerJaineOut();
    void on_pushScanBots_clicked();

    void TimerFindBotsOut();
    void on_pushScanBotsPause_clicked();


    void on_pushBags_clicked();
    void closeEvent(QCloseEvent*);

    void on_lineWaitMsLoadNode_cursorPositionChanged(int arg1, int arg2);

    void DBMwrite(QString msg);
    void show_message(QString text);
    void TimerControlHalion(QTimer &timer);
private:
    Ui::MainWindow *ui;
    WowObject *LocalPlayer;
    WowObject *LocalTarget;
    WowObject *CurrentObject;
    WowObject *TempObject;
    QTimer *Timer, *TimerReloadAdreses, *TimerIsLoading, TimerJaine, TimerFindBots;
    QSqlQueryModel  *model;

    QLabel *version;
};

#endif // MAINWINDOW_H
